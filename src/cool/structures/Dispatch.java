package cool.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Dispatch extends Symbol implements Expr {

	public boolean foundErrors = false;
	
	public Class clasa;
	public Expr cls;
	public Class staticClass;
	public String staticDispatch;
	public FunctionCall functionCall;
	
	
	public Scope parent;
	public String dispatchType;
	public Method calledMethod;
	
	public Formal wrongFormal;
	public Expr wrongArgument;
	
	private CoolParser.DispatchContext ctx;
	
	private int errorCode;
	
	public Dispatch(String name) {
		super(name);
	}
	
	public Dispatch(String staticDispatch, CoolParser.DispatchContext ctx) {
		super("dispatch");
		this.ctx = ctx;
		this.staticDispatch = staticDispatch;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
		case 1:
			errorMessage = "Undefined method " + this.functionCall.getName() +
				" in class " + this.clasa.getName();
			return new Error(this.getContext(), this.functionCall.getToken(), errorMessage);
		case 2:
			errorMessage = "Method " + this.functionCall.getName() +
				" of class " + this.clasa.getType() +
				" is applied to wrong number of arguments";
			return new Error(this.getContext(), this.functionCall.getToken(), errorMessage);
		case 3:
			errorMessage = "In call to method " + this.functionCall.getName() +
				" of class " + this.clasa.getType() +
				", actual type " + this.wrongArgument.getType() +
				" of formal parameter " + this.wrongFormal.getName() +
				" is incompatible with declared type " + this.wrongFormal.getType();
			return new Error(this.getContext(), this.wrongArgument.getToken(), errorMessage);
		case 4:
			errorMessage = "Type of static dispatch cannot be SELF_TYPE";
			return new Error(this.getContext(), this.ctx.cast().type().start, errorMessage);
		case 5:
			errorMessage = "Type " + this.staticDispatch + " of static dispatch is undefined";
			return new Error(this.getContext(), this.ctx.cast().type().start, errorMessage);
		case 6:
			errorMessage = "Type " + this.staticDispatch +
				" of static dispatch is not a superclass of type " + this.cls.getType();
			return new Error(this.getContext(), this.ctx.cast().type().start, errorMessage);
		default:
			return null;
		}
	}
	
	private boolean errorMethodNotFound() {
		if (this.clasa != null && this.calledMethod == null) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	private boolean errorWrongNumberOfArguments() {
		if (this.calledMethod != null &&
				this.calledMethod.formalsOrder.size() != this.functionCall.getNumberOfArguments()) {
			this.errorCode = 2;
			return true;
		}
		return false;
	}
	
	private boolean errorArgumentsTypeMissmatch() {
		if (this.calledMethod != null) {
			var callArguments = this.functionCall.arguments;
			var methodArguments = this.calledMethod.formalsOrder;
			for (int i = 0; i < callArguments.size(); i++) {
				callArguments.get(i).resolveTypes();
				String callArgumentType = callArguments.get(i).getType();
				String methodArgumentType = methodArguments.get(i).getType();
				
				if (! SymbolTable.primordial.checkIfIsInheritedType(callArgumentType, methodArgumentType)) {
					this.wrongFormal = methodArguments.get(i);
					this.wrongArgument = callArguments.get(i);
					this.errorCode = 3;
					return true;
				}
			}
		}
		return false;
	}
	
	public boolean errorSelfStaticDispatch() {
		if (this.staticDispatch != null && this.staticDispatch.equals("SELF_TYPE")) {
			this.errorCode = 4;
			return true;
		}
		return false;
	}
	
	public boolean errorUndefinedStaticDispatch() {
		if (this.staticDispatch != null &&
			SymbolTable.primordial.getClass(this.staticDispatch) == null)
		{
			this.errorCode = 5;
			return true;
		}
		return false;
	}
	
	public boolean erorrInheritanceStaticDispatch() {
		if (this.staticDispatch != null &&
			! SymbolTable.primordial.checkIfIsInheritedType(this.cls.getType(), this.staticDispatch))
			{
				this.errorCode = 6;
				return true;
			}
			return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors ||
			errorSelfStaticDispatch();
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorUndefinedStaticDispatch() ||
			erorrInheritanceStaticDispatch() ||
			errorMethodNotFound() ||
			errorWrongNumberOfArguments() ||
			errorArgumentsTypeMissmatch();
		
		return this.foundErrors;
	}
	
	public void accept(ErrorVisitor visitor) {
		this.resolveTypes();
		visitor.visit(this);
		this.cls.accept(visitor);
		this.functionCall.accept(visitor);
		
		var disp = this;
//		System.out.println(disp + " " + disp.cls.getType() + "." + disp.getFunctionCall() + " => " + disp.calledMethod + " | " + disp.getType());
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent == null) {
			return null;
		}
		return parent.lookupForType(str);
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.dispatchType;
	}
	
	public void setType(String dispatchType) {
		this.dispatchType = dispatchType;
	}
	
	public void setCls(Expr cls) {
		cls.setParent(this);
		this.cls = cls;
	}
	
	public Expr getCls() {
		return this.cls;
	}
	
	public String getClassName() {
		return this.clasa.getType();
	}
	
	public void setFunctionCall(FunctionCall functionCall) {
		functionCall.setParent(this);
		this.functionCall = functionCall;
	}
	
	public FunctionCall getFunctionCall() {
		return this.functionCall;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		this.cls.resolveTypes();
		this.functionCall.resolveTypes();
		if (this.staticDispatch != null) {
			this.clasa = SymbolTable.primordial.getClass(staticDispatch);
		}
		else if (this.cls.toString().equals("self")) {
			var parent = this.getParent();
			while (!(parent instanceof Class)) {
				parent = parent.getParent();
			}
			this.clasa = (Class) parent;
		} else {
			this.clasa = SymbolTable.primordial.getClass(cls.getType());
		}
		if (clasa != null && clasa.lookupMethod(this.functionCall.getName()) != null) {
			this.calledMethod = clasa.lookupMethod(this.functionCall.getName());
			if (this.calledMethod != null) {
				this.setType(this.calledMethod.getType());
			}
		}
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
	
	public ParserRuleContext getContext() {
		return this.ctx;
	}
}
