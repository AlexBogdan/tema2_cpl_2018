package cool.structures;

import java.io.File;
import java.util.LinkedList;

import org.antlr.v4.runtime.*;

import cool.compiler.Compiler;
import cool.parser.CoolParser;

public class SymbolTable {
    public static Scope globals;
    
    private static LinkedList<Error> errors = new LinkedList<>();
    private static LinkedList<Scope> garbage = new LinkedList<Scope>();
    
    private static boolean semanticErrors;
    
    public static Primordial primordial;
    
    public static void defineBasicClasses() {
        globals = new DefaultScope(null);
        semanticErrors = false;
        
        // TODO Populate global scope.
        globals.addType("Object");
        globals.addType("IO");
        globals.addType("Int");
        globals.addType("String");
        globals.addType("Bool");
    }
    
    /**
     * Displays a semantic error message.
     * 
     * @param ctx Used to determine the enclosing class context of this error,
     *            which knows the file name in which the class was defined.
     * @param info Used for line and column information.
     * @param str The error message.
     */
    public static void error(ParserRuleContext ctx, Token info, String str) {
        while (! (ctx.getParent() instanceof CoolParser.ProgramContext))
            ctx = ctx.getParent();
        
        String message = "\"" + new File(Compiler.fileNames.get(ctx)).getName()
                + "\", line " + info.getLine()
                + ":" + (info.getCharPositionInLine() + 1)
                + ", Semantic error: " + str;
        
        System.err.println(message);
        
        semanticErrors = true;
    }
    
    public static void error(String str) {
        String message = "Semantic error: " + str;
        
        System.err.println(message);
        
        semanticErrors = true;
    }
    
    public static boolean hasSemanticErrors() {
        return semanticErrors;
    }
    
    public static void addError(Error error) {
    	if (error != null) {
    		errors.add(error);
    	}
    }
    
    public static void addGarbage(Scope element) {
    	garbage.add(element);
    }
    
    public static void throwErrors() {
    	for (Error error : errors) {
    		error.throwError();
    	}
    	errors.clear();
    	
    	for (Scope element : garbage) {
    		if (element.endErrors()) {
    			element.getError().throwError();
    		}
    	}
    	garbage.clear();
    }
}
