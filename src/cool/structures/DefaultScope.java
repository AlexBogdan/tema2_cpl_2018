package cool.structures;

import java.util.*;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;

public class DefaultScope implements Scope {
    
    public Set<String> types = new HashSet<>();
    
    private Scope parent;
    
    public DefaultScope(Scope parent) {
        this.parent = parent;
    }

    @Override
    public boolean addType(String type) {
        // Reject duplicates in the same scope.
        if (types.contains(type))
            return false;
        
        types.add(type);
        
        return true;
    }

    @Override
    public String lookupForType(String name) {
        if (this.types.contains(name)) {
        	return name;
        }
        
//        if (parent != null)
//            return parent.lookup(name);
        
        return null;
    }

    @Override
    public Scope getParent() {
        return parent;
    }
    
    @Override
    public String toString() {
        return types.toString();
    }

	@Override
	public boolean endErrors() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void accept(ErrorVisitor visitor) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public Error getError() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean initErrors() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		
	}
	
	public Token getToken() {
		return null;
	}
}
