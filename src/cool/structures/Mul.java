package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Mul extends Symbol implements Expr {

	private boolean foundErrors = false;
	
	private Expr left = null;
	private Expr right = null;
	private Scope parent = null;
	private String mulType = "Int";
	private CoolParser.MulContext ctx;
	
	private int errorCode = 0;
	
	public Mul(CoolParser.MulContext ctx) {
		super("*");
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
		case 1:
			errorMessage = "Operand of " + name +
				" has type " + this.left.getType() +
				" instead of Int";
			return new Error(ctx, ctx.start, errorMessage); 
		case 2:
			
			errorMessage = "Operand of " + name +
				" has type " + this.right.getType() +
				" instead of Int";
			return new Error(ctx, ctx.start, errorMessage); 
		}
		return null;
	}

	private boolean errorOperationType() {
		if (! this.left.getType().equals("Int")) {
			this.errorCode = 1;
			return true;
		}
		if (! this.right.getType().equals("Int")) {
			this.errorCode = 2;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors;
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorOperationType();

		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
		this.left.accept(visitor);
		this.right.accept(visitor);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent != null) {
			return parent.lookupForType(str);
		}
		return null;
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.mulType;
	}
	
	public void setType(String mulType) {
		this.mulType = mulType;
	}
	
	public void setLeft(Expr left) {
		left.setParent(this);
		this.left = left;
	}
	
	public Expr getLeft() {
		return this.left;
	}
	
	public void setRight(Expr right) {
		right.setParent(this);
		this.right = right;
	}
	
	public Expr getRight() {
		return this.right;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
