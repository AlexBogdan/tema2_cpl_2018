package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Assignment extends Symbol implements Expr {

	private boolean foundErrors = false;
	
	private Id left = null;
	private Expr right = null;
	private Scope parent = null;
	private String equalType = null;
	private CoolParser.AssignmentContext ctx;
	
	private int errorCode = 0;
	
	public Assignment(CoolParser.AssignmentContext ctx) {
		super("assignment");
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
		case 1:
			errorMessage = "Cannot assign to self";
			return new Error(ctx, this.getToken(), errorMessage);
		case 2:
			errorMessage = "Type " + this.right.getType() +
				" of assigned expression is incompatible with declared type " + 
				this.left.getType() + " of identifier " + this.left.getName();
			return new Error(ctx, this.right.getToken(), errorMessage);
		}
		return null;
	}

	private boolean errorSelfAssignment() {
		if (this.left.getName().equals("self")) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	private boolean errorTypeAssignment() {
		if (SymbolTable.globals.lookupForType(this.right.getType()) != null &&
			! this.left.getType().equals(this.right.getType()) &&
			! SymbolTable.primordial.checkIfIsInheritedType(right.getType(), left.getType()))
		{
			this.errorCode = 2;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors ||
			errorSelfAssignment();
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorTypeAssignment();

		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		this.right.resolveTypes();
		visitor.visit(this);
		this.left.accept(visitor);
		this.right.accept(visitor);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent != null) {
			return parent.lookupForType(str);
		}
		return null;
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.equalType;
	}
	
	public void setType(String equalType) {
		this.equalType = equalType;
	}
	
	public void setLeft(Id left) {
		left.setParent(this);
		this.left = left;
	}
	
	public Expr getLeft() {
		return this.left;
	}
	
	public void setRight(Expr right) {
		right.setParent(this);
		this.right = right;
	}
	
	public Expr getRight() {
		return this.right;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		this.right.resolveTypes();
		this.setType(this.right.getType());
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
