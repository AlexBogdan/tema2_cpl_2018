package cool.structures;

import java.util.HashMap;
import java.util.LinkedList;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public final class Primordial extends Symbol implements Scope {
	
	private HashMap<String, Class> classes;
	private CoolParser.ProgramContext ctx;
	
	public Primordial(String name) {
		super(name);
		this.ctx = null;
	}
	
	public Primordial(CoolParser.ProgramContext ctx) {
		super("BaseClass");
		this.classes = new HashMap<>();
		this.ctx = ctx;
	}
	
	public Error getError() {
		return null;
	}

	@Override
	public boolean addType(String sym) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		// TODO Auto-generated method stub
		for (var cls : this.classes.values()) {
			if (cls.getName().equals(str)) {
				return cls.getType();
			}
		}
		return null;
	}
	
	public boolean checkIfIsInheritedType (String startType, String endType) {
		if (startType == null || endType == null) {
			return false;
		}
		if (startType.equals(endType) || endType.equals("Object")) {
			return true;
		}
		
		var cls = this.classes.get(startType);
		while (cls != null && cls.getParent() != null) {
			if (cls.getParent().getType().equals(endType)) {
				return true;
			}
			cls = cls.getParent();
		}
		return false;
	}
	
	public boolean isBaseType(String type) {
		return type.equals("Int") ||
			type.equals("Bool") ||
			type.equals("String");
	}
	
	public LinkedList<String> parentChain(String type) {
		LinkedList<String> parentChain = new LinkedList<>();
		
		parentChain.addLast(type);
		var cls = this.classes.get(type);
		while (cls.getParent() != null) {
			parentChain.addLast(cls.getParent().getType());
			cls = classes.get(cls.getParent().getType());
		}
				
		return parentChain;
	}
	
	public String getCommonParent(String type1, String type2) {
		String commonParent = "Object";
		
		if (this.isBaseType(type1) || this.isBaseType(type2)) {
			if (type1.equals(type2)) {
				return type1;
			} else {
				return commonParent;
			}
		}
				
		LinkedList<String> parentChain1 = this.parentChain(type1);
		LinkedList<String> parentChain2 = this.parentChain(type2);
				
		while (! parentChain1.isEmpty() &&
			! parentChain2.isEmpty() &&
			parentChain1.getLast().equals(parentChain2.getLast()))
		{
			commonParent = parentChain1.getLast();
			parentChain1.removeLast();
			parentChain2.removeLast();
		}
		
		return commonParent;	
	}

	@Override
	public Scope getParent() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean endErrors() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public void accept(ErrorVisitor visitor) {
		// TODO Auto-generated method stub
		for (var cls : this.classes.values()) {
			cls.accept(visitor);
		}
	}
	
	public void addClass(Class cls) {
		this.classes.put(cls.getName(), cls);
	}
	
	public Class getClass(String cls) {
		return this.classes.get(cls);
	}
	
	public HashMap<String, Class> getClasses() {
		return this.classes;
	}

	@Override
	public boolean initErrors() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
