package cool.structures;

import java.util.ArrayList;
import java.util.HashMap;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class If extends Symbol implements Expr {

	public boolean foundErrors = false;
	
	private Scope parent;
	private String ifType = null;
	
	private Expr ifCondition = null;
	private Expr ifBody = null;
	private Expr ifElse = null;
	
	private CoolParser.IfContext ctx;
	
	private int errorCode;
	
	public If(CoolParser.IfContext ctx) {
		super("if");
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
			case 1:
				errorMessage = "If condition has type " +
					this.ifCondition.getType() + " instead of Bool";
				return new Error(ctx, this.ifCondition.getToken(), errorMessage);
			default:
				return null;
		}
	}
	
	private boolean errorConditionType() {
		if (! this.ifCondition.getType().equals("Bool")) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorConditionType();
		
		return this.foundErrors;
	}
	
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
		if (this.ifCondition != null) {
			this.ifCondition.accept(visitor);
		}
		if (this.ifBody != null) {
			this.ifBody.accept(visitor);
		}
		if (this.ifElse != null) {
			this.ifElse.accept(visitor);
		}
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent == null) {
			return null;
		}
		return parent.lookupForType(str);
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		if (this.ifType == null)
			this.resolveTypes();
		return this.ifType;
	}
	
	public void setType(String ifType) {
		this.ifType = ifType;
	}
	
	public void setIfCondition(Expr ifCondition) {
		ifCondition.setParent(this);
		if (ifCondition.initErrors()) {
			SymbolTable.addGarbage(ifCondition);
		}
		this.ifCondition = ifCondition;
	}
	
	public Expr getIfCondition() {
		return this.ifCondition;
	}
	
	public void setIfBody(Expr ifBody) {
		ifBody.setParent(this);
		if (ifBody.initErrors()) {
			SymbolTable.addGarbage(ifBody);
		}
		this.ifBody = ifBody;
	}
	
	public Expr getIfBody() {
		return this.ifBody;
	}
	
	public void setIfElse(Expr ifElse) {
		ifElse.setParent(this);
		if (ifElse.initErrors()) {
			SymbolTable.addGarbage(ifElse);
		}
		this.ifElse = ifElse;
	}
	
	public Expr getIfElse() {
		return this.ifElse;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		this.setType(SymbolTable.primordial.getCommonParent(this.ifBody.getType(), this.ifElse.getType()));
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
