package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;

public interface Scope {
    public boolean addType(String sym);
    
    public String lookupForType(String str);
    
    public Scope getParent();
    
    public boolean initErrors();
    
    public boolean endErrors();
    
    public String getType();
    
    public void resolveTypes();
    
    public void accept(ErrorVisitor visitor);
    
    public Error getError();
    
    public Token getToken();
}
