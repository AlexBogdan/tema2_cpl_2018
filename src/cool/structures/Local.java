package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Local extends Symbol implements Scope {

	private boolean foundErrors;
	
	private Let parent = null;
	private String localType;
	private Expr initialValue;
	
	private int errorCode = 0;
	
	private CoolParser.LocalContext ctx;
	
	public Local(String localName, String localType, CoolParser.LocalContext ctx) {
		super(localName);
		this.localType = localType;
		this.ctx = ctx;
		this.initialValue = null;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
			case 1:
				errorMessage = "Let variable has illegal name " + name;	
				return new Error(ctx, ctx.varName.start, errorMessage);
			case 2:
				errorMessage = "Let variable " + name + " has undefined type " + localType;
				return new Error(ctx, ctx.varType.start, errorMessage);
			case 3:
				errorMessage = "Type " + this.initialValue.getType() +
					" of initialization expression of identifier " + this.getName() +
					" is incompatible with declared type " + this.getType();
				return new Error(ctx, this.initialValue.getToken(), errorMessage);
			default:
				return null;
		}
	}
	
	private boolean errorLegalName() {
		if (name.equals("self")) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	private boolean errorUndefinedType() {
		if (SymbolTable.globals.lookupForType(localType) == null) {
			this.errorCode = 2;
			return true;
		}
		return false;
	}
	
	private boolean errorInitializatonType() {
		if (this.initialValue != null &&
			! SymbolTable.primordial.checkIfIsInheritedType(initialValue.getType(), this.getType())) {
			this.errorCode = 3;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors ||
			errorLegalName();
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorUndefinedType() ||
			errorInitializatonType();
		
		return this.foundErrors;
	}
	
	public void accept(ErrorVisitor visitor) {
		this.resolveTypes();
		visitor.visit(this);
		if (initialValue != null) {
			visitor.visit(this.initialValue);
		}
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		// TODO Auto-generated method stub
		if (this.name.equals(str))
			return this.getType();
		return parent.lookupForType(str);
	}

	@Override
	public Let getParent() {
		return parent;
	}
	
	public void setParent(Let parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.localType;
	}
	
	public void setType(String localType) {
		this.localType = localType;
	}
	
	public CoolParser.LocalContext getContext() {
		return this.ctx;
	}
	
	public Expr getInitialValue() {
		return this.initialValue;
	}
	
	public void setInitialValue(Expr initialValue) {
		if (initialValue == null)
			return;
		initialValue.setParent(this);
		if (initialValue.initErrors()) {
			SymbolTable.addGarbage(initialValue);
		}
		this.initialValue = initialValue;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		if (this.initialValue != null)
			this.initialValue.resolveTypes();
	} 
	
	public Token getToken() {
		return this.ctx.start;
	}
}
