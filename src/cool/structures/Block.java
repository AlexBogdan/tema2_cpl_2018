package cool.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Block extends Symbol implements Expr {

	public LinkedList<Expr> statements;
	
	private Scope parent;
	private String blockType;
	
	private CoolParser.BlockContext ctx;
	
	public Block(CoolParser.BlockContext ctx) {
		super("");
		this.ctx = ctx;
		this.statements = new LinkedList<>();
	}

	public Error getError() {
		return null;
	}
	
	private boolean methodRedefinition() {
//		if (parent.internMethod(name)) {
//			String errorMessage = "Class " + parent.getName() + " redefines method " + name;
//			
//			SymbolTable.error(ctx, ctx.methodName.start, errorMessage);
//			
//			return true;
//		}
		return false;
	}
	
	public boolean initErrors() {
		return false;
	}
	
	public boolean endErrors() {
		return false;
	}
	
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
		for (var expr : this.statements) {
			expr.accept(visitor);
		}
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent == null)
			return null;
		return parent.lookupForType(str);
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.blockType;
	}
	
	public void setType(String blockType) {
		this.blockType = blockType;
	}
	
	public void addStatement(Expr statement) {
		statement.setParent(this);
		if (statement.initErrors()) {
//			SymbolTable.addGarbage(local);
		}
		this.statements.add(statement);
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
//		this.letType = this.letBody.getType();
		for (var statement : this.statements) {
			statement.resolveTypes();
		}
		this.setType(this.statements.getLast().getType());
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
