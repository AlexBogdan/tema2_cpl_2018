package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Equal extends Symbol implements Expr {

	private boolean foundErrors = false;
	
	private Expr left = null;
	private Expr right = null;
	private Scope parent = null;
	private String equalType = "Bool";
	private CoolParser.EqualContext ctx;
	
	private int errorCode = 0;
	
	public Equal(CoolParser.EqualContext ctx) {
		super("compare");
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
		case 1:
			errorMessage = "Cannot compare " + this.left.getType() + " with " + this.right.getType();
			return new Error(ctx, this.getToken(), errorMessage);
		}
		return null;
	}

	private boolean isBaseType(String type) {
		return type.equals("Int") ||
			type.equals("String") ||
			type.equals("Bool");
	}
	
	private boolean errorOperationType() {
		if ((isBaseType(this.left.getType()) || isBaseType(this.right.getType())) &&
				! this.left.getType().equals(this.right.getType())) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors;
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorOperationType();

		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
		this.left.accept(visitor);
		this.right.accept(visitor);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent != null) {
			return parent.lookupForType(str);
		}
		return null;
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.equalType;
	}
	
	public void setType(String equalType) {
		this.equalType = equalType;
	}
	
	public void setLeft(Expr left) {
		left.setParent(this);
		this.left = left;
	}
	
	public Expr getLeft() {
		return this.left;
	}
	
	public void setRight(Expr right) {
		right.setParent(this);
		this.right = right;
	}
	
	public Expr getRight() {
		return this.right;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		
	}
	
	public Token getToken() {
		return this.ctx.EQUAL().getSymbol();
	}
}
