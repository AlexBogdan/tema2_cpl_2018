package cool.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Let extends Symbol implements Expr {

	public boolean foundErrors = false;
	
	public LinkedList<Local> allLocals;
	public HashMap<String, Local> locals;
	
	private Scope parent;
	private String letType;
	private Expr letBody;
	
	private CoolParser.LetContext ctx;
	
	public Let(CoolParser.LetContext ctx) {
		super("let");
		this.ctx = ctx;
		this.locals = new HashMap<>();
		this.allLocals = new LinkedList<>();
		this.letBody = null;
	}

	public Error getError() {
		return null;
	}
	
	private boolean methodRedefinition() {
//		if (parent.internMethod(name)) {
//			String errorMessage = "Class " + parent.getName() + " redefines method " + name;
//			
//			SymbolTable.error(ctx, ctx.methodName.start, errorMessage);
//			
//			return true;
//		}
		return false;
	}
	
	public boolean initErrors() {
		return methodRedefinition();
	}
	
	public boolean endErrors() {
		return foundErrors;
	}
	
	public void accept(ErrorVisitor visitor) {
		for (var local : this.locals.values()) {
			local.accept(visitor);
		}
		if (this.letBody != null)
			this.letBody.accept(visitor);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		Local local = getLocal(str);
		if (local != null) {
			return local.getType();
		}
		if (parent == null) {
			return null;
		}
		return parent.lookupForType(str);
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.letType;
	}
	
	public void setType(String letType) {
		this.letType = letType;
	}
	
	public void addLocal(Local local) {
		local.setParent(this);
		if (local.initErrors()) {
			SymbolTable.addGarbage(local);
		}
		this.locals.put(local.getName(), local);
		this.allLocals.add(local);
	}
	
	public Local getLocal(String localName) {
		for (var local : this.allLocals) {
			if (local.getName().equals(localName) && ! local.initErrors()) {
				return local;
			}
		}
		return this.locals.get(localName);
	}
	
	public boolean internLocal(String localName) {
		for (var local : this.allLocals) {
			if (local.getName().equals(localName)) {
				return true;
			}
		}
		return false;
	}
	
	public void setLetBody(Expr expr) {
		expr.setParent(this);
		if (expr.initErrors()) {
			SymbolTable.addGarbage(expr);
		}
		this.letBody = expr;
	}
	
	public Expr getLetBody() {
		return this.letBody;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		this.letBody.resolveTypes();
		this.letType = this.letBody.getType();
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
