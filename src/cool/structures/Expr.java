package cool.structures;

public interface Expr extends Scope {
    public Scope getParent();
    
    public void setParent(Scope element);
}
