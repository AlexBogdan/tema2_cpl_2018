package cool.structures;

import java.util.ArrayList;
import java.util.HashMap;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Method extends Symbol implements Scope {

	public boolean foundErrors;
	
	public HashMap<String, Formal> formals;
	public ArrayList<Formal> formalsOrder;
	
	private Class parent;
	private Expr methodBody;
	private String methodType;
	private CoolParser.MethodContext ctx;
	
	private int errorCode = 0;
	private Formal errorLocalFormal = null;
	private Formal errorInheritedFormal = null;
	private Method errorInheritedMethod = null;
	
	public Method(String methodName, String methodType, CoolParser.MethodContext ctx) {
		super(methodName);
		this.methodType = methodType;
		this.parent = null;
		this.ctx = ctx;
		this.formals = new HashMap<>();
		this.formalsOrder = new ArrayList<>();
	}
	
	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
		case 1:
			errorMessage = "Class " + parent.getName() + " redefines method " + name;
			return new Error(ctx, ctx.methodName.start, errorMessage);
		case 2:
		case 3:
			errorMessage = "Class " + parent.getName() +
				" overrides method " + name +
				" with different number of formal parameters";
	
			return new Error(ctx, ctx.methodName.start, errorMessage);
		case 4:
			errorMessage = "Class " + parent.getName() +
				" overrides method " + name +
				" but changes return type from " +
				errorInheritedMethod.getType() + " to " + this.methodType;
	
			return new Error(ctx, ctx.methodType.start, errorMessage);
		case 5:
			errorMessage = "Class " + parent.getName() +
				" overrides method " + name +
				" but changes type of formal parameter " + errorLocalFormal.getName() +
				" from " + errorInheritedFormal.getType() + " to " + errorLocalFormal.getType();
	
			return new Error(ctx, errorLocalFormal.getContext().formalType.start, errorMessage);
		case 6:
			errorMessage = "Type " + this.methodBody.getType() +
				" of the body of method " + this.getName() +
				" is incompatible with declared return type " + this.getType();
			return new Error(ctx, this.methodBody.getToken(), errorMessage);
		default:
			return null;
		}
	}
	
	private boolean errorMethodRedefinition() {
		if (parent.internMethod(name)) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	private boolean errorInheritanceFormalsNumber() {
		if (parent.getParent() != null) {
			Method inheritedMethod = parent.getParent().lookupMethod(name);
			
			if (inheritedMethod != null &&
				inheritedMethod.formalsOrder.size() != this.formalsOrder.size()) 
			{
				this.errorCode = 3;
				this.errorInheritedMethod = inheritedMethod;
				return true;
			}
		}
		return false;
	}
	
	private boolean errorInheritanceOverridesType() {
		if (parent.getParent() != null) {
			Method inheritedMethod = parent.getParent().lookupMethod(name);
			
			if (inheritedMethod != null && ! inheritedMethod.getType().equals(this.methodType)) {
				this.errorCode = 4;
				this.errorInheritedMethod = inheritedMethod;
				return true;
			}
		}
		return false;
	}
	
	private boolean errorInheritanceOverridesFormal() {
		if (parent.getParent() != null) {
			Method inheritedMethod = parent.getParent().lookupMethod(name);
						
			if (inheritedMethod != null) {
				for (int i = 0; i < formalsOrder.size(); i++) {
					Formal inheritedFormal = inheritedMethod.getFormal(i);
					Formal localFormal = this.getFormal(i);
									
					if (localFormal != null && ! inheritedFormal.getType().equals(localFormal.getType())) {
						this.errorCode = 5;
						this.errorLocalFormal = localFormal;
						this.errorInheritedFormal = inheritedFormal;
						this.errorInheritedMethod = inheritedMethod;
						return true;
					}
				}
			}
		}
		return false;
	}
	
	private boolean errorReturnType() {
//		System.out.println(name + " | " + methodBody);
		if (! this.getType().equals("Object") &&
			! SymbolTable.primordial.checkIfIsInheritedType(methodBody.getType(), this.getType())) {
			this.errorCode = 6;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors ||
			errorMethodRedefinition();
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorInheritanceFormalsNumber() ||
			errorInheritanceOverridesType() ||
			errorInheritanceOverridesFormal() ||
			errorReturnType();
		
		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		this.resolveTypes();
		visitor.visit(this);
		for (var formal : this.formals.values()) {
			formal.accept(visitor);
		}
		if (methodBody != null)
			this.methodBody.accept(visitor);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (this.formals.containsKey(str)) {
			return this.formals.get(str).getType();
		}
		return this.parent.lookupForType(str);
	}

	@Override
	public Class getParent() {
		return parent;
	}
	
	public void setParent(Class parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.methodType;
	}
	
	public void setType(String methodType) {
		this.methodType = methodType;
	}
	
	public void addFormal(Formal formal) {
		formal.setParent(this);
		if (formal.initErrors()) {
			SymbolTable.addGarbage(formal);
		}
		this.formals.putIfAbsent(formal.getName(), formal);
		this.formalsOrder.add(formal);
	}
	
	public Formal getFormal(String formal) {
		return this.formals.get(formal);
	}
	
	public Formal getFormal(int index) {
		return this.formalsOrder.get(index);
	}
	
	public boolean internFormal(String formalName) {
		return this.formals.containsKey(formalName);
	}
	
	public void setMethodBody(Expr methodBody) {
		methodBody.setParent(this);
		if (methodBody.initErrors()) {
			SymbolTable.addGarbage(methodBody);
		}
		this.methodBody = methodBody;
	}
	
	public Expr getMethodBody() {
		return this.methodBody;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		for (Formal formal : this.formalsOrder) {
			formal.resolveTypes();
		}
		this.methodBody.resolveTypes();
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
