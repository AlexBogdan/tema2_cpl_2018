package cool.structures;

import java.util.HashMap;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Class extends Symbol implements Scope {

	private boolean foundErrors = false;
	
	public HashMap<String, Attribute> attributes;
	public HashMap<String, Method> methods;
	
	private Class parent;
	private String parentName;	
	private CoolParser.ClassDeclarationContext ctx;
	
	private int errorCode = 0;
	
	public Class(String className, String parentName, CoolParser.ClassDeclarationContext ctx) {
		super(className);
		this.parentName = parentName;
		this.parent = null;
		this.ctx = ctx;
		this.attributes = new HashMap<>();
		this.methods = new HashMap<>();
		
		this.foundErrors = initErrors();
		SymbolTable.globals.addType(this.name);
	}
	
	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
			case 1:
				errorMessage = "Class has illegal name " + name;
				return new Error(ctx, ctx.className.start, errorMessage);
			case 2:
				errorMessage = "Class " + name + " is redefined";
				return new Error(ctx, ctx.className.start, errorMessage);
			case 3:
				errorMessage = "Class " + name + " has illegal parent " + parentName;
				return new Error(ctx, ctx.classInherited.start, errorMessage);
			case 4:
				errorMessage = "Class " + name + " has undefined parent " + parentName;
				return new Error(ctx, ctx.classInherited.start, errorMessage);
			case 5:
				errorMessage = "Inheritance cycle for class " + name;
				return new Error(ctx, ctx.className.start, errorMessage);
			default:
				return null;
		}
		
	}
	
	private boolean errorLegalName() {
		if (name.equals("SELF_TYPE")) {
			this.errorCode = 1;
			return true;
		}
		
		return false;
	}
	
	private boolean errorClassRedefinition() {
		if (SymbolTable.globals.lookupForType(name) != null) {
			this.errorCode = 2;
			return true;
		}
		
		return false;
	}
	
	private boolean errorIllegalParent() {
		if (parentName != null &&
			(
				parentName.equals("Int") ||
				parentName.equals("Bool") ||
				parentName.equals("String") ||
				parentName.equals("IO") ||
				parentName.equals("SELF_TYPE")
			)
		) {
			this.errorCode = 3;
			return true;
		}
		
		return false;
	}

	private boolean errorUndefinedParent() {
		if (parentName != null &&
			SymbolTable.globals.lookupForType(parentName) == null
		) {
			this.errorCode = 4;
			return true;
		}
		
		return false;
	}
	
	private boolean checkInheritanceCycle(String start) {
		if (parentName == null || parent == null) {
			return false;
		}
		if (parentName.equals(start)) {
			return true;
		}
		
		return getParent().checkInheritanceCycle(start);
	}
	
	private boolean errorInheritanceCycle() {
		if (checkInheritanceCycle(name)) {
			this.errorCode = 5;
			return true;
		}
		
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors ||
			errorLegalName() ||
			errorIllegalParent() ||
			errorClassRedefinition();
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorUndefinedParent() ||
			errorInheritanceCycle();
		
		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
		for (var attr : this.attributes.values()) {
			attr.accept(visitor);
		}
		for (var method : this.methods.values()) {
			method.accept(visitor);
		}
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (this.attributes.containsKey(str)) {
			return this.attributes.get(str).getType();
		}
		if (this.methods.containsKey(str)) {
			return this.methods.get(str).getType();
		}
		if (parent != null) {
			return parent.lookupForType(str);
		}
		return null;
	}

	@Override
	public Class getParent() {
		return parent;
	}
	
	public void setParent(Class parent) {
		this.parent = parent;
	}
	
	public String getParentName() {
		return parentName;
	}
	
	public void addAttribute(Attribute attr) {
		attr.setParent(this);
		if (! attr.initErrors()) {
			this.attributes.put(attr.getName(), attr);
		} else {
			SymbolTable.addError(attr.getError());
		}
	}
	
	public boolean internAttribute(String attr) {
		return this.attributes.containsKey(attr);
	}
	
	public boolean inheritedAttribute(String attr) {
		return parent != null && (parent.internAttribute(attr) || parent.inheritedAttribute(attr));
	}
	
	public void addMethod(Method method) {
		method.setParent(this);
		if (! method.initErrors()) {
			this.methods.put(method.getName(), method);
		} else {
			SymbolTable.addError(method.getError());
		}
	}
	
	public Method getMethod(String method) {
		return this.methods.get(method);
	}
	
	public Method lookupMethod(String method) {
		if (methods.containsKey(method)) {
			return methods.get(method);
		}
		else if (parent == null) {
			return null;
		} else {
			return parent.lookupMethod(method);
		}
	}
	
	public boolean internMethod(String method) {
		return this.methods.containsKey(method);
	}

	@Override
	public String getType() {
		// TODO Auto-generated method stub
		return name;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		for (var method : this.methods.values()) {
			method.resolveTypes();
		}
		for (var attribute : this.attributes.values()) {
			attribute.resolveTypes();
		}
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
