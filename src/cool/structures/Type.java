package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Type extends Symbol implements Expr {

	private boolean foundErrors = false;
	
	private Scope parent = null;
	private String typeType = null;
	private CoolParser.TypeContext ctx;
	
	private int errorCode = 0;
	
	public Type(String typeName, CoolParser.TypeContext ctx) {
		super(typeName);
		this.ctx = ctx;
	}

	public Error getError() {
		return null;
	}

	public boolean initErrors() {
		this.foundErrors = foundErrors;
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.resolveTypes();
		this.foundErrors = foundErrors;
		
		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		// TODO Auto-generated method stub
		return this.parent.lookupForType(str);
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		if (this.typeType == null) {
			this.resolveTypes();
		}
		return this.typeType;
	}
	
	public void setType(String typeType) {
		this.typeType = typeType;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		this.setType(SymbolTable.primordial.getClass(this.name).getName());
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
