package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class CaseBranch extends Symbol implements Scope {

	private boolean foundErrors = false;
	
	private Case parent = null;
	private String caseBranchType;
	private CoolParser.CaseBranchContext ctx;
	private Expr impliedExpr = null;
	
	private int errorCode = 0;
	
	public CaseBranch(String caseBranchName, String caseBranchType, CoolParser.CaseBranchContext ctx) {
		super(caseBranchName);
		this.caseBranchType = caseBranchType;
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
		case 1:
			errorMessage = "Case variable has illegal name " + name;
			return new Error(ctx, ctx.varName.start, errorMessage);
		case 2:
			errorMessage = "Case variable " + name + " has illegal type " + caseBranchType;
			return new Error(ctx, ctx.varType.start, errorMessage);
		case 3:
			errorMessage = "Case variable " + name + " has undefined type " + caseBranchType;
			return new Error(ctx, ctx.varType.start, errorMessage);
		default:
			return null;
		}
	}
	
	private boolean errorLegalName() {
		if (name.equals("self")) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	private boolean errorIllegalType() {
		if (caseBranchType.equals("SELF_TYPE")) {
			this.errorCode = 2;
			return true;
		}
		return false;
	}
	
	private boolean errorUndefinedType() {
		if (SymbolTable.globals.lookupForType(caseBranchType) == null) {
			this.errorCode = 3;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors ||
			errorLegalName() ||
			errorIllegalType();
		
		return foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorUndefinedType();
		
		return foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		// TODO Auto-generated method stub
		if (parent == null) {
			return null;
		}
		return parent.lookupForType(str);
	}

	@Override
	public Case getParent() {
		return parent;
	}
	
	public void setParent(Case parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.caseBranchType;
	}
	
	public void setType(String caseBranchType) {
		this.caseBranchType = caseBranchType;
	}
	
	public void setImpliedExpr(Expr impliedExpr) {
		impliedExpr.setParent(this);
		this.impliedExpr = impliedExpr;
	}
	
	public Expr getImpliedExpr() {
		return this.impliedExpr;
	}
	
	public CoolParser.CaseBranchContext getContext() {
		return this.ctx;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		this.impliedExpr.resolveTypes();
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
