package cool.structures;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;

import cool.parser.CoolParser;

public class ImplicitDispatch extends Dispatch {
	
	public CoolParser.ImplicitDispatchContext ctx;
	
	public ImplicitDispatch(CoolParser.ImplicitDispatchContext ctx) {
		super("Implicit Dispatch");
		this.ctx = ctx;
		this.staticDispatch = null;
		this.cls = new Id("self", null);
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
	
	public ParserRuleContext getContext() {
		return this.ctx;
	}
}
