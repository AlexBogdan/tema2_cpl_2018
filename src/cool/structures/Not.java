package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Not extends Symbol implements Expr {

	private boolean foundErrors = false;
	
	private Expr right = null;
	private Scope parent = null;
	private String notType = "Bool";
	private CoolParser.NotContext ctx;
	
	private int errorCode = 0;
	
	public Not(CoolParser.NotContext ctx) {
		super("not");
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
		case 1:
			errorMessage = "Operand of " + name +
				" has type " + this.right.getType() +
				" instead of Bool";
			return new Error(ctx, this.right.getToken(), errorMessage); 
		}
		return null;
	}

	private boolean errorOperationType() {
		if (! this.right.getType().equals("Bool")) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors;
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorOperationType();

		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
		this.right.accept(visitor);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent != null) {
			return parent.lookupForType(str);
		}
		return null;
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.notType;
	}
	
	public void setType(String notType) {
		this.notType = notType;
	}
	
	public void setRight(Expr right) {
		right.setParent(this);
		this.right = right;
	}
	
	public Expr getRight() {
		return this.right;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
