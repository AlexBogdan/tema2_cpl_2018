package cool.structures;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class FunctionCall extends Symbol implements Expr {

	public boolean foundErrors = false;
	
	public LinkedList<Expr> arguments;
	
	private Scope parent;
	private String functionCallType;
	
	private CoolParser.FunctionCallContext ctx;
	
	public FunctionCall(String functionCallName, CoolParser.FunctionCallContext ctx) {
		super(functionCallName);
		this.ctx = ctx;
		this.arguments = new LinkedList<>();
	}

	public Error getError() {
		return null;
	}
	
	private boolean methodRedefinition() {
//		if (parent.internMethod(name)) {
//			String errorMessage = "Class " + parent.getName() + " redefines method " + name;
//			
//			SymbolTable.error(ctx, ctx.methodName.start, errorMessage);
//			
//			return true;
//		}
		return false;
	}
	
	public boolean initErrors() {
		return methodRedefinition();
	}
	
	public boolean endErrors() {
		return foundErrors;
	}
	
	public void accept(ErrorVisitor visitor) {
		this.resolveTypes();
		visitor.visit(this);
		for (var arg : this.arguments) {
			arg.accept(visitor);
		}
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent == null) {
			return null;
		}
		return parent.lookupForType(str);
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.functionCallType;
	}
	
	public void setType(String functionCallType) {
		this.functionCallType = functionCallType;
	}
	
	public void addArgument(Expr arg) {
		arg.setParent(this);
		if (arg.initErrors()) {
			SymbolTable.addGarbage(arg);
		}
		this.arguments.add(arg);
	}
	
	public int getNumberOfArguments() {
		return this.arguments.size();
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		for (var arg : this.arguments) {
			arg.resolveTypes();
		}
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
