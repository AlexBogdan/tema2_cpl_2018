package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Formal extends Symbol implements Scope {

	private boolean foundErrors = false;
	
	private Method parent;
	private String formalType;
	private CoolParser.FormalContext ctx;
	
	private int errorCode = 0;
	
	public Formal(String formalName, String formalType, CoolParser.FormalContext ctx) {
		super(formalName);
		this.formalType = formalType;
		this.parent = null;
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
		case 1:
			errorMessage = "Method " + parent.getName() +
				" of class " + parent.getParent().getName() +
				" has formal parameter with illegal name " + name;
			return new Error(ctx, ctx.formalName.start, errorMessage);
		case 2:
			errorMessage = "Method " + parent.getName() +
				" of class " + parent.getParent().getName() +
				" redefines formal parameter " + name;
			return new Error(ctx, ctx.formalName.start, errorMessage);
		case 3:
			errorMessage = "Method " + parent.getName() +
				" of class " + parent.getParent().getName() +
				" has formal parameter " + name +
				" with illegal type " + formalType;
			return new Error(ctx, ctx.formalType.start, errorMessage);
		case 4:
			errorMessage = "Method " + parent.getName() +
				" of class " + parent.getParent().getName() +
				" has formal parameter " + name +
				" with undefined type " + formalType;
			return new Error(ctx, ctx.formalType.start, errorMessage);
		default:
			return null;
		}
	}
	
	private boolean errorLegalName() {
		if (name.equals("self")) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	private boolean errorFormalRedefinition() {
		if (parent.internFormal(name)) {
			this.errorCode = 2;
			return true;
		}
		return false;
	}
	
	private boolean errorIllegalType() {
		if (formalType.equals("SELF_TYPE")) {
			this.errorCode = 3;
			return true;
		}
		return false;
	}
	
	private boolean errorUndefinedType() {
		if (SymbolTable.globals.lookupForType(formalType) == null) {
			this.errorCode = 4;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors ||
			errorLegalName() ||
			errorFormalRedefinition() ||
			errorIllegalType();
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorUndefinedType();
		
		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		// TODO Auto-generated method stub
		if (this.name.equals(str)) {
			return this.getType();
		}
		return null;
	}

	@Override
	public Method getParent() {
		return parent;
	}
	
	public void setParent(Method parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.formalType;
	}
	
	public void setType(String formalType) {
		this.formalType = formalType;
	}
	
	public CoolParser.FormalContext getContext() {
		return this.ctx;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
