package cool.structures;

import java.util.ArrayList;
import java.util.HashMap;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class While extends Symbol implements Expr {

	public boolean foundErrors = false;
	
	private Scope parent;
	private String whileType = "Object";
	
	private Expr whileCondition = null;
	private Expr whileBody = null;
	
	private CoolParser.WhileContext ctx;
	
	private int errorCode;
	
	public While(CoolParser.WhileContext ctx) {
		super("");
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
			case 1:
				errorMessage = "While condition has type " +
					this.whileCondition.getType() + " instead of Bool";
				return new Error(ctx, this.whileCondition.getToken(), errorMessage);
			default:
				return null;
		}
	}
	
	private boolean errorConditionType() {
		if (! this.whileCondition.getType().equals("Bool")) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorConditionType();
		
		return this.foundErrors;
	}
	
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
		if (this.whileCondition != null) {
			this.whileCondition.accept(visitor);
		}
		if (this.whileBody != null) {
			this.whileBody.accept(visitor);
		}
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent == null) {
			return null;
		}
		return parent.lookupForType(str);
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.whileType;
	}
	
	public void setType(String whileType) {
		this.whileType = whileType;
	}
	
	public void setWhileCondition(Expr whileCondition) {
		whileCondition.setParent(this);
		if (whileCondition.initErrors()) {
			SymbolTable.addGarbage(whileCondition);
		}
		this.whileCondition = whileCondition;
	}
	
	public Expr getWhileCondition() {
		return this.whileCondition;
	}
	
	public void setWhileBody(Expr whileBody) {
		whileBody.setParent(this);
		if (whileBody.initErrors()) {
			SymbolTable.addGarbage(whileBody);
		}
		this.whileBody = whileBody;
	}
	
	public Expr getWhileBody() {
		return this.whileBody;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
//		this.whileType = this.whileBody.getType();
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
