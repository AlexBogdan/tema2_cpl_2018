package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class New extends Symbol implements Expr {

	private boolean foundErrors = false;
	
	private Scope parent = null;
	private CoolParser.NewContext ctx;
	private String newType;
	private CoolParser.TypeContext typeCtx;
	
	private int errorCode = 0;
	
	public New(CoolParser.NewContext ctx) {
		super("new");
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage = null;
		switch (this.errorCode) {
		case 1:
			errorMessage = "new is used with undefined type " + this.getType();
			return new Error(ctx, typeCtx.start, errorMessage); 
		}
		return null;
	}

	private boolean errorUndefinedType() {
		if (SymbolTable.globals.lookupForType(this.getType()) == null) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors;
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorUndefinedType();

		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent != null) {
			return parent.lookupForType(str);
		}
		return null;
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.newType;
	}
	
	public void setType(String newType) {
		this.newType = newType;
	}
	
	public void setNewType(CoolParser.TypeContext ctx, String type) {
		this.typeCtx = ctx;
		this.setType(type);
	}
	
	public void setRight(Expr right) {
		
	}
	
	public Expr getRight() {
		return null;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
