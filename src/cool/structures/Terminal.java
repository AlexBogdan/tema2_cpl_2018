package cool.structures;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Terminal extends Symbol implements Expr {

	private boolean foundErrors = false;
	
	private Scope parent = null;
	private String terminalType = null;
	private ParserRuleContext ctx;
	
	private int errorCode = 0;
	
	public Terminal(String terminalValue, String terminalType, ParserRuleContext ctx) {
		super(terminalValue);
		this.terminalType = terminalType;
		this.ctx = ctx;
	}

	public Error getError() {
		return null;
	}

	public boolean initErrors() {
		this.foundErrors = foundErrors;
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors;;
		
		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.terminalType;
	}
	
	public void setType(String terminalType) {}

	@Override
	public boolean addType(String sym) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
