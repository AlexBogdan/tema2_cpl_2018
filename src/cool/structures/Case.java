package cool.structures;

import java.util.ArrayList;
import java.util.HashMap;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Case extends Symbol implements Expr {

	public boolean foundErrors = false;
	
	public Expr condition;
	public HashMap<String, CaseBranch> caseBranches;
	
	private Scope parent;
	private String caseType;
	
	private CoolParser.CaseContext ctx;
	
	public Case(CoolParser.CaseContext ctx) {
		super("case");
		this.ctx = ctx;
		this.caseBranches = new HashMap<>();
	}

	public Error getError() {
		return null;
	}
	
	private boolean methodRedefinition() {
//		if (name.equals("self")) {
//			String errorMessage = "Case variable has illegal name " + name;
//			
//			SymbolTable.error(ctx, ctx.methodName.start, errorMessage);
//			
//			return true;
//		}
		return false;
	}
	
	public boolean initErrors() {
		return methodRedefinition();
	}
	
	public boolean endErrors() {
		return foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		this.resolveTypes();
		if (this.condition != null)
			this.condition.accept(visitor);
		for (var branch : this.caseBranches.values()) {
			branch.accept(visitor);
		}
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		// TODO Auto-generated method stub
		if (parent == null) {
			return null;
		}
		return parent.lookupForType(str);
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
//		if (this.caseType == null) {
//			this.resolveTypes();
//		}
		return this.caseType;
	}
	
	public void setType(String caseType) {
		this.caseType = caseType;
	}
	
	public void addCaseBranch(CaseBranch caseBranch) {
		caseBranch.setParent(this);
		if (caseBranch.initErrors()) {
			SymbolTable.addGarbage(caseBranch);
		}
		this.caseBranches.put(caseBranch.getName(), caseBranch);
	}
	
	public CaseBranch getCaseBranch(String caseBranch) {
		return this.caseBranches.get(caseBranch);
	}
	
	public boolean internCaseBranch(String caseBranch) {
		return this.caseBranches.containsKey(caseBranch);
	}
	
	public void setCondition(Expr condition) {
		condition.setParent(this);
		this.condition = condition;
	}
	
	public Expr getCondition() {
		return this.condition;
	}

	@Override
	public void resolveTypes() {
//		De cautat cel mai comun stramos intre branch-uri
		ArrayList<String> branchTypes = new ArrayList<>();
		for (var branch : this.caseBranches.values()) {
			branch.resolveTypes();
			branchTypes.add(branch.getImpliedExpr().getType());
		}
		
		String resultType = branchTypes.get(0);
		for (int i = 1; i < branchTypes.size(); i++) {
			resultType = SymbolTable.primordial.getCommonParent(resultType, branchTypes.get(i));
		}
		
		this.setType(resultType);
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
