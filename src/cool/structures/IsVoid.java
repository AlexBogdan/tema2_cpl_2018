package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class IsVoid extends Symbol implements Expr {

	private Expr right = null;
	private Scope parent = null;
	private String isVoidType = "Bool";
	private CoolParser.IsvoidContext ctx;
	
	public IsVoid(CoolParser.IsvoidContext ctx) {
		super("isvoid");
		this.ctx = ctx;
	}

	public Error getError() {
		return null;
	}
	
	public boolean initErrors() {
		return false;
	}
	
	public boolean endErrors() {
		return false;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
		this.right.accept(visitor);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		if (parent != null) {
			return parent.lookupForType(str);
		}
		return null;
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.isVoidType;
	}
	
	public void setType(String isVoidType) {
		this.isVoidType = isVoidType;
	}
	
	public void setRight(Expr right) {
		right.setParent(this);
		this.right = right;
	}
	
	public Expr getRight() {
		return this.right;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
