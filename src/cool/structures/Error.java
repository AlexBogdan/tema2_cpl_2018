package cool.structures;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.Token;

public class Error {
	private String message;
	private Token token;
	private ParserRuleContext ctx;
	
	public Error(ParserRuleContext ctx, Token token, String message) {
		this.message = message;
		this.token = token;
		this.ctx = ctx;
	}
	
	public void throwError() {
		SymbolTable.error(ctx, token, message);
	}
}
