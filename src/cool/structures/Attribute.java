package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Attribute extends Symbol implements Scope {

	private boolean foundErrors = false;
	
	private Class parent = null;
	private String attrType;
	private Expr attrInitialValue = null;
	private CoolParser.AttributeContext ctx;
	
	private int errorCode = 0;
	
	public Attribute(String attrName, String attrType, CoolParser.AttributeContext ctx) {
		super(attrName);
		this.attrType = attrType;
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage = null;
		switch(this.errorCode) {
		case 1:
			errorMessage = "Class " + parent.getName() +
				" has attribute with illegal name " + name;
			return new Error(ctx, ctx.attrName.start, errorMessage);
		case 2:
			errorMessage = "Class " + parent.getName() + " redefines attribute " + name;
			return new Error(ctx, ctx.attrName.start, errorMessage);
		case 3:
			errorMessage = "Class " + parent.getName() + " redefines inherited attribute " + name;
			return new Error(ctx, ctx.attrName.start, errorMessage);
		case 4:
			errorMessage = "Class " + parent.getName() +
				" has attribute " + name +
				" with undefined type " + attrType;
			return new Error(ctx, ctx.attrType.start, errorMessage);
		case 5:
			errorMessage = "Type " + this.attrInitialValue.getType() +
				" of initialization expression of attribute " + this.getName() +
				" is incompatible with declared type " + this.getType();
			return new Error(ctx, this.attrInitialValue.getToken(), errorMessage);
		default:
			return null;
		}
	}
	
	private boolean errorLegalName() {
		if (name.equals("self")) {
			this.errorCode = 1;
			return true;
		}
		return false;
	}
	
	private boolean errorRedefinedAttribute() {
		if (parent.internAttribute(name)) {
			this.errorCode = 2;
			return true;
		}
		return false;
	}
	
	private boolean errorRedefinedInheritedAttribute() {
		if (parent.inheritedAttribute(name)) {
			this.errorCode = 3;
			return true;
		}
		return false;
	}
	
	private boolean errorUndefinedType() {
		if (SymbolTable.globals.lookupForType(attrType) == null) {
			this.errorCode = 4;
			return true;
		}
		return false;
	}
	
	private boolean errorInitializatonType() {
		if (this.attrInitialValue != null &&
			! SymbolTable.primordial.checkIfIsInheritedType(attrInitialValue.getType(), this.getType())) {
			this.errorCode = 5;
			return true;
		}
		return false;
	}
	
	public boolean initErrors() {
		this.foundErrors = foundErrors ||
			errorLegalName() ||
			errorRedefinedAttribute();
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.foundErrors = foundErrors ||
			errorRedefinedInheritedAttribute() ||
			errorUndefinedType() ||
			errorInitializatonType();
		
		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
		if (this.attrInitialValue != null) {
			this.attrInitialValue.accept(visitor);
		}
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		// TODO Auto-generated method stub
		if (this.name.equals(str)) {
			return this.getType();
		}
		if (parent != null) {
			return parent.lookupForType(str);
		}
		return null;
	}

	@Override
	public Class getParent() {
		return parent;
	}
	
	public void setParent(Class parent) {
		this.parent = parent;
	}
	
	public String getType() {
		return this.attrType;
	}
	
	public void setType(String attrType) {
		this.attrType = attrType;
	}
	
	public void setIinitialValue(Expr initialValue) {
		initialValue.setParent(this);
		this.attrInitialValue = initialValue;
	}
	
	public Expr getInitialValue() {
		return this.attrInitialValue;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		if (this.attrInitialValue != null)
			this.attrInitialValue.resolveTypes();
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
