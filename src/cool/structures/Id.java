package cool.structures;

import org.antlr.v4.runtime.Token;

import cool.compiler.ErrorVisitor;
import cool.parser.CoolParser;

public class Id extends Symbol implements Expr {

	private boolean foundErrors = false;
	
	private Scope parent = null;
	private String idType = null;
	private CoolParser.IdContext ctx;
	
	private int errorCode = 0;
	
	public Id(String idName, CoolParser.IdContext ctx) {
		super(idName);
		this.ctx = ctx;
	}

	public Error getError() {
		String errorMessage;
		switch (this.errorCode) {
			case 1:
				errorMessage = "Undefined identifier " + name;
				return new Error(ctx, ctx.start, errorMessage);
			default:
				return null;
		}
	}
	
	private boolean errorUndefinedId() {
		if (! name.equals("self") && parent != null) {
			if (parent instanceof Local) {
				if (parent.getParent().lookupForType(name) == null ||
					((Local) parent).getName().equals(name))
				{
					this.errorCode = 1;
					return true;
				}
			}
			else if (parent.lookupForType(name) == null) {
				this.errorCode = 1;
				return true;
			}
		}
		return false;
	}

	public boolean initErrors() {
		this.foundErrors = foundErrors;
		
		return this.foundErrors;
	}
	
	public boolean endErrors() {
		this.resolveTypes();
		this.foundErrors = foundErrors ||
			errorUndefinedId();
		
		return this.foundErrors;
	}
	
	@Override
	public void accept(ErrorVisitor visitor) {
		visitor.visit(this);
	}
	
	@Override
	public boolean addType(String type) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public String lookupForType(String str) {
		// TODO Auto-generated method stub
		return this.parent.lookupForType(str);
	}

	@Override
	public Scope getParent() {
		return parent;
	}
	
	public void setParent(Scope parent) {
		this.parent = parent;
	}
	
	public String getType() {
		if (this.idType == null) {
			this.resolveTypes();
		}
		return this.idType;
	}
	
	public void setType(String idType) {
		this.idType = idType;
	}

	@Override
	public void resolveTypes() {
		// TODO Auto-generated method stub
		if (this.parent != null) {
			if (this.getName().equals("self")) {
				this.setType(parent.getType());
			}
			else {
				this.setType(parent.lookupForType(this.name));
			}
		}
	}
	
	public Token getToken() {
		return this.ctx.start;
	}
}
