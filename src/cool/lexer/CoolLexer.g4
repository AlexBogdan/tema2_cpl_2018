lexer grammar CoolLexer;

tokens { ERROR } 

@header{
    package cool.lexer;	
    
//    import java.util.Stack;
}

@members{    
    private void raiseError(String msg) {
        setText(msg);
        setType(ERROR);
    }
   	
   	private String string;
//   	private String comment = null;
//   	Stack<String> comments = new Stack<>();
}

COMM_OPEN		:	'(*' -> pushMode(IN_COMM), more;
COMM_CLOSED		:	'*)'	{
	raiseError("Unmatched *)");
};

STR_OPEN 		:	'"' 	{
	string = "";
} -> pushMode(IN_STR), more;



// Definim cuvintele cheie (constante)
CLASS			:	'class';
ELSE			:	'else';
FI				:	'fi';
IF				:	'if';
IN				:	'in';
INHERITS		:	'inherits';
ISVOID			:	'isvoid';
LET				:	'let';
LOOP			:	'loop';
POOL			:	'pool';
THEN			:	'then';
WHILE			:	'while';
CASE			:	'case';
ESAC			:	'esac';
NEW				:	'new';
OF				:	'of';

TRUE			:	'true';
FALSE			:	'false';

// Definim operatorul de atribuire
ASSIGNMENT		:	'<-';

// Definim operatorii aritmetici
PLUS			:	'+';
MINUS			:	'-';
MUL				:	'*';
DIV				:	'/';
COMPLEMENT		:	'~';

// Definim operatorii logici
LESS			:	'<';
LESSEQUAL		:	'<=';
EQUAL			:	'=';
NOT				:	'not';

// Definim operatorii de dispatch
POINT			:	'.';
AROND			:	'@';

// Definim alte caractere necesare
OPENED_CURL_BRACKET		:	'{';
CLOSED_CURL_BRACKET		:	'}';
OPENED_ROUND_BRACKET		:	'(';
CLOSED_ROUND_BRACKET		:	')';
SEMICOLON		:	';';
COLON			:	':';
COMMA			:	',';
IMPLIES			:	'=>';

// Definim tipurile
SELFTYPE		:	'SELF_TYPE';
CLASSNAME		:	[A-Z](LETTER | DIGIT | '_')*;

ID				:	([a-z] | '_')+(LETTER | DIGIT | '_')*;

// Definim primitivele
fragment ZERO			:	[0];
fragment DIGIT			: 	[0-9];
fragment NONZERODIGIT	:	[1-9];
INTEGER			:	ZERO | MINUS? NONZERODIGIT DIGIT*;
BOOL			:	TRUE | FALSE;
LETTER			: 	[a-zA-Z];

SIMPLE_COMM : '--' .*?'\n' -> skip;

//BLOCK_COMMENT
//    : '(*'
//      (BLOCK_COMMENT|.)*?
//      ('*)' | EOF {System.err.println("EOF in comment");}) -> skip
//    ;

CACAT			:	'~~~';

WS
    :   [ \n\f\r\t]+ -> skip
    ;

CHAR 		: 	.	{
	raiseError("Invalid character: " + getText());
};
    
mode IN_STR;

STR_EOF		:	EOF			{
	raiseError("EOF in string constant");
} -> popMode;
STR_NEWLINE	:	. '\n'		{
	if (getText().charAt(0) != '\\')
		raiseError("Unterminated string constant");
} -> popMode;
STRING		:	'"' 		{
	if (string.length() > 1024)
		raiseError("String constant too long");
	else if (string.contains("\u0000"))
		raiseError("String contains null character");
	else
		setText(string);
} -> popMode;
TAB			:	'\\t' 		{
	string += "\t";
} -> more;
NEW_L		:	'\\n' 		{
	string += "\n";
} -> more;
FORM_FEED	:	'\\f' 		{
	string += "\f";
} -> more;
BACKSPACE	:	'\\b' 		{
	string += "\b";
} -> more;
SLASH_CHAR	:	'\\' ~('t' | 'n' | 'f' | 'b') {
	string += getText().charAt(1);
} -> more;
STR_CHAR 		: 	('\\"' | ~'"')	{
	string += getText();
} -> more;


mode IN_COMM;
COMM_EOF		:	EOF			{
	raiseError("EOF in comment");
} -> popMode;
OPENED_COMMENT	:	'(*'	-> pushMode(IN_COMM), more;
BLOCK_COMMENT	:	'*)'	-> popMode, more;
COMM_CHAR 		: 	.		-> more;