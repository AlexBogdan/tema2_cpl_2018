// Generated from CoolLexer.g4 by ANTLR 4.7.1

    package cool.lexer;	
    
//    import java.util.Stack;

import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class CoolLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		ERROR=1, COMM_CLOSED=2, CLASS=3, ELSE=4, FI=5, IF=6, IN=7, INHERITS=8, 
		ISVOID=9, LET=10, LOOP=11, POOL=12, THEN=13, WHILE=14, CASE=15, ESAC=16, 
		NEW=17, OF=18, TRUE=19, FALSE=20, ASSIGNMENT=21, PLUS=22, MINUS=23, MUL=24, 
		DIV=25, COMPLEMENT=26, LESS=27, LESSEQUAL=28, EQUAL=29, NOT=30, POINT=31, 
		AROND=32, OPENED_CURL_BRACKET=33, CLOSED_CURL_BRACKET=34, OPENED_ROUND_BRACKET=35, 
		CLOSED_ROUND_BRACKET=36, SEMICOLON=37, COLON=38, COMMA=39, IMPLIES=40, 
		SELFTYPE=41, CLASSNAME=42, ID=43, INTEGER=44, BOOL=45, LETTER=46, SIMPLE_COMM=47, 
		CACAT=48, WS=49, CHAR=50, STR_EOF=51, STR_NEWLINE=52, STRING=53, COMM_EOF=54, 
		COMM_OPEN=55;
	public static final int
		IN_STR=1, IN_COMM=2;
	public static String[] channelNames = {
		"DEFAULT_TOKEN_CHANNEL", "HIDDEN"
	};

	public static String[] modeNames = {
		"DEFAULT_MODE", "IN_STR", "IN_COMM"
	};

	public static final String[] ruleNames = {
		"COMM_OPEN", "COMM_CLOSED", "STR_OPEN", "CLASS", "ELSE", "FI", "IF", "IN", 
		"INHERITS", "ISVOID", "LET", "LOOP", "POOL", "THEN", "WHILE", "CASE", 
		"ESAC", "NEW", "OF", "TRUE", "FALSE", "ASSIGNMENT", "PLUS", "MINUS", "MUL", 
		"DIV", "COMPLEMENT", "LESS", "LESSEQUAL", "EQUAL", "NOT", "POINT", "AROND", 
		"OPENED_CURL_BRACKET", "CLOSED_CURL_BRACKET", "OPENED_ROUND_BRACKET", 
		"CLOSED_ROUND_BRACKET", "SEMICOLON", "COLON", "COMMA", "IMPLIES", "SELFTYPE", 
		"CLASSNAME", "ID", "ZERO", "DIGIT", "NONZERODIGIT", "INTEGER", "BOOL", 
		"LETTER", "SIMPLE_COMM", "CACAT", "WS", "CHAR", "STR_EOF", "STR_NEWLINE", 
		"STRING", "TAB", "NEW_L", "FORM_FEED", "BACKSPACE", "SLASH_CHAR", "STR_CHAR", 
		"COMM_EOF", "OPENED_COMMENT", "BLOCK_COMMENT", "COMM_CHAR"
	};

	private static final String[] _LITERAL_NAMES = {
		null, null, null, "'class'", "'else'", "'fi'", "'if'", "'in'", "'inherits'", 
		"'isvoid'", "'let'", "'loop'", "'pool'", "'then'", "'while'", "'case'", 
		"'esac'", "'new'", "'of'", "'true'", "'false'", "'<-'", "'+'", "'-'", 
		"'*'", "'/'", "'~'", "'<'", "'<='", "'='", "'not'", "'.'", "'@'", "'{'", 
		"'}'", "'('", "')'", "';'", "':'", "','", "'=>'", "'SELF_TYPE'", null, 
		null, null, null, null, null, "'~~~'"
	};
	private static final String[] _SYMBOLIC_NAMES = {
		null, "ERROR", "COMM_CLOSED", "CLASS", "ELSE", "FI", "IF", "IN", "INHERITS", 
		"ISVOID", "LET", "LOOP", "POOL", "THEN", "WHILE", "CASE", "ESAC", "NEW", 
		"OF", "TRUE", "FALSE", "ASSIGNMENT", "PLUS", "MINUS", "MUL", "DIV", "COMPLEMENT", 
		"LESS", "LESSEQUAL", "EQUAL", "NOT", "POINT", "AROND", "OPENED_CURL_BRACKET", 
		"CLOSED_CURL_BRACKET", "OPENED_ROUND_BRACKET", "CLOSED_ROUND_BRACKET", 
		"SEMICOLON", "COLON", "COMMA", "IMPLIES", "SELFTYPE", "CLASSNAME", "ID", 
		"INTEGER", "BOOL", "LETTER", "SIMPLE_COMM", "CACAT", "WS", "CHAR", "STR_EOF", 
		"STR_NEWLINE", "STRING", "COMM_EOF", "COMM_OPEN"
	};
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	    
	    private void raiseError(String msg) {
	        setText(msg);
	        setType(ERROR);
	    }
	   	
	   	private String string;
	//   	private String comment = null;
	//   	Stack<String> comments = new Stack<>();


	public CoolLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "CoolLexer.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getChannelNames() { return channelNames; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	@Override
	public void action(RuleContext _localctx, int ruleIndex, int actionIndex) {
		switch (ruleIndex) {
		case 1:
			COMM_CLOSED_action((RuleContext)_localctx, actionIndex);
			break;
		case 2:
			STR_OPEN_action((RuleContext)_localctx, actionIndex);
			break;
		case 53:
			CHAR_action((RuleContext)_localctx, actionIndex);
			break;
		case 54:
			STR_EOF_action((RuleContext)_localctx, actionIndex);
			break;
		case 55:
			STR_NEWLINE_action((RuleContext)_localctx, actionIndex);
			break;
		case 56:
			STRING_action((RuleContext)_localctx, actionIndex);
			break;
		case 57:
			TAB_action((RuleContext)_localctx, actionIndex);
			break;
		case 58:
			NEW_L_action((RuleContext)_localctx, actionIndex);
			break;
		case 59:
			FORM_FEED_action((RuleContext)_localctx, actionIndex);
			break;
		case 60:
			BACKSPACE_action((RuleContext)_localctx, actionIndex);
			break;
		case 61:
			SLASH_CHAR_action((RuleContext)_localctx, actionIndex);
			break;
		case 62:
			STR_CHAR_action((RuleContext)_localctx, actionIndex);
			break;
		case 63:
			COMM_EOF_action((RuleContext)_localctx, actionIndex);
			break;
		}
	}
	private void COMM_CLOSED_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 0:

				raiseError("Unmatched *)");

			break;
		}
	}
	private void STR_OPEN_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 1:

				string = "";

			break;
		}
	}
	private void CHAR_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 2:

				raiseError("Invalid character: " + getText());

			break;
		}
	}
	private void STR_EOF_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 3:

				raiseError("EOF in string constant");

			break;
		}
	}
	private void STR_NEWLINE_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 4:

				if (getText().charAt(0) != '\\')
					raiseError("Unterminated string constant");

			break;
		}
	}
	private void STRING_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 5:

				if (string.length() > 1024)
					raiseError("String constant too long");
				else if (string.contains("\u0000"))
					raiseError("String contains null character");
				else
					setText(string);

			break;
		}
	}
	private void TAB_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 6:

				string += "\t";

			break;
		}
	}
	private void NEW_L_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 7:

				string += "\n";

			break;
		}
	}
	private void FORM_FEED_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 8:

				string += "\f";

			break;
		}
	}
	private void BACKSPACE_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 9:

				string += "\b";

			break;
		}
	}
	private void SLASH_CHAR_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 10:

				string += getText().charAt(1);

			break;
		}
	}
	private void STR_CHAR_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 11:

				string += getText();

			break;
		}
	}
	private void COMM_EOF_action(RuleContext _localctx, int actionIndex) {
		switch (actionIndex) {
		case 12:

				raiseError("EOF in comment");

			break;
		}
	}

	public static final String _serializedATN =
		"\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\29\u01c6\b\1\b\1\b"+
		"\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n"+
		"\t\n\4\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21"+
		"\4\22\t\22\4\23\t\23\4\24\t\24\4\25\t\25\4\26\t\26\4\27\t\27\4\30\t\30"+
		"\4\31\t\31\4\32\t\32\4\33\t\33\4\34\t\34\4\35\t\35\4\36\t\36\4\37\t\37"+
		"\4 \t \4!\t!\4\"\t\"\4#\t#\4$\t$\4%\t%\4&\t&\4\'\t\'\4(\t(\4)\t)\4*\t"+
		"*\4+\t+\4,\t,\4-\t-\4.\t.\4/\t/\4\60\t\60\4\61\t\61\4\62\t\62\4\63\t\63"+
		"\4\64\t\64\4\65\t\65\4\66\t\66\4\67\t\67\48\t8\49\t9\4:\t:\4;\t;\4<\t"+
		"<\4=\t=\4>\t>\4?\t?\4@\t@\4A\tA\4B\tB\4C\tC\4D\tD\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\3\4\3\4\3\4\3\5\3\5\3\5\3\5\3\5\3"+
		"\5\3\6\3\6\3\6\3\6\3\6\3\7\3\7\3\7\3\b\3\b\3\b\3\t\3\t\3\t\3\n\3\n\3\n"+
		"\3\n\3\n\3\n\3\n\3\n\3\n\3\13\3\13\3\13\3\13\3\13\3\13\3\13\3\f\3\f\3"+
		"\f\3\f\3\r\3\r\3\r\3\r\3\r\3\16\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17"+
		"\3\17\3\20\3\20\3\20\3\20\3\20\3\20\3\21\3\21\3\21\3\21\3\21\3\22\3\22"+
		"\3\22\3\22\3\22\3\23\3\23\3\23\3\23\3\24\3\24\3\24\3\25\3\25\3\25\3\25"+
		"\3\25\3\26\3\26\3\26\3\26\3\26\3\26\3\27\3\27\3\27\3\30\3\30\3\31\3\31"+
		"\3\32\3\32\3\33\3\33\3\34\3\34\3\35\3\35\3\36\3\36\3\36\3\37\3\37\3 \3"+
		" \3 \3 \3!\3!\3\"\3\"\3#\3#\3$\3$\3%\3%\3&\3&\3\'\3\'\3(\3(\3)\3)\3*\3"+
		"*\3*\3+\3+\3+\3+\3+\3+\3+\3+\3+\3+\3,\3,\3,\3,\7,\u0131\n,\f,\16,\u0134"+
		"\13,\3-\6-\u0137\n-\r-\16-\u0138\3-\3-\3-\7-\u013e\n-\f-\16-\u0141\13"+
		"-\3.\3.\3/\3/\3\60\3\60\3\61\3\61\5\61\u014b\n\61\3\61\3\61\7\61\u014f"+
		"\n\61\f\61\16\61\u0152\13\61\5\61\u0154\n\61\3\62\3\62\5\62\u0158\n\62"+
		"\3\63\3\63\3\64\3\64\3\64\3\64\7\64\u0160\n\64\f\64\16\64\u0163\13\64"+
		"\3\64\3\64\3\64\3\64\3\65\3\65\3\65\3\65\3\66\6\66\u016e\n\66\r\66\16"+
		"\66\u016f\3\66\3\66\3\67\3\67\3\67\38\38\38\38\38\39\39\39\39\39\39\3"+
		":\3:\3:\3:\3:\3;\3;\3;\3;\3;\3;\3;\3<\3<\3<\3<\3<\3<\3<\3=\3=\3=\3=\3"+
		"=\3=\3=\3>\3>\3>\3>\3>\3>\3>\3?\3?\3?\3?\3?\3?\3@\3@\3@\5@\u01ac\n@\3"+
		"@\3@\3@\3@\3A\3A\3A\3A\3A\3B\3B\3B\3B\3B\3B\3C\3C\3C\3C\3C\3C\3D\3D\3"+
		"D\3D\3\u0161\2E\59\7\4\t\2\13\5\r\6\17\7\21\b\23\t\25\n\27\13\31\f\33"+
		"\r\35\16\37\17!\20#\21%\22\'\23)\24+\25-\26/\27\61\30\63\31\65\32\67\33"+
		"9\34;\35=\36?\37A C!E\"G#I$K%M&O\'Q(S)U*W+Y,[-]\2_\2a\2c.e/g\60i\61k\62"+
		"m\63o\64q\65s\66u\67w\2y\2{\2}\2\177\2\u0081\2\u00838\u0085\2\u0087\2"+
		"\u0089\2\5\2\3\4\13\3\2C\\\4\2aac|\3\2\62\62\3\2\62;\3\2\63;\4\2C\\c|"+
		"\5\2\13\f\16\17\"\"\6\2ddhhppvv\3\2$$\2\u01ce\2\5\3\2\2\2\2\7\3\2\2\2"+
		"\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2\2\2\23\3"+
		"\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2\35\3\2\2"+
		"\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2\2)\3\2\2"+
		"\2\2+\3\2\2\2\2-\3\2\2\2\2/\3\2\2\2\2\61\3\2\2\2\2\63\3\2\2\2\2\65\3\2"+
		"\2\2\2\67\3\2\2\2\29\3\2\2\2\2;\3\2\2\2\2=\3\2\2\2\2?\3\2\2\2\2A\3\2\2"+
		"\2\2C\3\2\2\2\2E\3\2\2\2\2G\3\2\2\2\2I\3\2\2\2\2K\3\2\2\2\2M\3\2\2\2\2"+
		"O\3\2\2\2\2Q\3\2\2\2\2S\3\2\2\2\2U\3\2\2\2\2W\3\2\2\2\2Y\3\2\2\2\2[\3"+
		"\2\2\2\2c\3\2\2\2\2e\3\2\2\2\2g\3\2\2\2\2i\3\2\2\2\2k\3\2\2\2\2m\3\2\2"+
		"\2\2o\3\2\2\2\3q\3\2\2\2\3s\3\2\2\2\3u\3\2\2\2\3w\3\2\2\2\3y\3\2\2\2\3"+
		"{\3\2\2\2\3}\3\2\2\2\3\177\3\2\2\2\3\u0081\3\2\2\2\4\u0083\3\2\2\2\4\u0085"+
		"\3\2\2\2\4\u0087\3\2\2\2\4\u0089\3\2\2\2\5\u008b\3\2\2\2\7\u0091\3\2\2"+
		"\2\t\u0096\3\2\2\2\13\u009c\3\2\2\2\r\u00a2\3\2\2\2\17\u00a7\3\2\2\2\21"+
		"\u00aa\3\2\2\2\23\u00ad\3\2\2\2\25\u00b0\3\2\2\2\27\u00b9\3\2\2\2\31\u00c0"+
		"\3\2\2\2\33\u00c4\3\2\2\2\35\u00c9\3\2\2\2\37\u00ce\3\2\2\2!\u00d3\3\2"+
		"\2\2#\u00d9\3\2\2\2%\u00de\3\2\2\2\'\u00e3\3\2\2\2)\u00e7\3\2\2\2+\u00ea"+
		"\3\2\2\2-\u00ef\3\2\2\2/\u00f5\3\2\2\2\61\u00f8\3\2\2\2\63\u00fa\3\2\2"+
		"\2\65\u00fc\3\2\2\2\67\u00fe\3\2\2\29\u0100\3\2\2\2;\u0102\3\2\2\2=\u0104"+
		"\3\2\2\2?\u0107\3\2\2\2A\u0109\3\2\2\2C\u010d\3\2\2\2E\u010f\3\2\2\2G"+
		"\u0111\3\2\2\2I\u0113\3\2\2\2K\u0115\3\2\2\2M\u0117\3\2\2\2O\u0119\3\2"+
		"\2\2Q\u011b\3\2\2\2S\u011d\3\2\2\2U\u011f\3\2\2\2W\u0122\3\2\2\2Y\u012c"+
		"\3\2\2\2[\u0136\3\2\2\2]\u0142\3\2\2\2_\u0144\3\2\2\2a\u0146\3\2\2\2c"+
		"\u0153\3\2\2\2e\u0157\3\2\2\2g\u0159\3\2\2\2i\u015b\3\2\2\2k\u0168\3\2"+
		"\2\2m\u016d\3\2\2\2o\u0173\3\2\2\2q\u0176\3\2\2\2s\u017b\3\2\2\2u\u0181"+
		"\3\2\2\2w\u0186\3\2\2\2y\u018d\3\2\2\2{\u0194\3\2\2\2}\u019b\3\2\2\2\177"+
		"\u01a2\3\2\2\2\u0081\u01ab\3\2\2\2\u0083\u01b1\3\2\2\2\u0085\u01b6\3\2"+
		"\2\2\u0087\u01bc\3\2\2\2\u0089\u01c2\3\2\2\2\u008b\u008c\7*\2\2\u008c"+
		"\u008d\7,\2\2\u008d\u008e\3\2\2\2\u008e\u008f\b\2\2\2\u008f\u0090\b\2"+
		"\3\2\u0090\6\3\2\2\2\u0091\u0092\7,\2\2\u0092\u0093\7+\2\2\u0093\u0094"+
		"\3\2\2\2\u0094\u0095\b\3\4\2\u0095\b\3\2\2\2\u0096\u0097\7$\2\2\u0097"+
		"\u0098\b\4\5\2\u0098\u0099\3\2\2\2\u0099\u009a\b\4\6\2\u009a\u009b\b\4"+
		"\3\2\u009b\n\3\2\2\2\u009c\u009d\7e\2\2\u009d\u009e\7n\2\2\u009e\u009f"+
		"\7c\2\2\u009f\u00a0\7u\2\2\u00a0\u00a1\7u\2\2\u00a1\f\3\2\2\2\u00a2\u00a3"+
		"\7g\2\2\u00a3\u00a4\7n\2\2\u00a4\u00a5\7u\2\2\u00a5\u00a6\7g\2\2\u00a6"+
		"\16\3\2\2\2\u00a7\u00a8\7h\2\2\u00a8\u00a9\7k\2\2\u00a9\20\3\2\2\2\u00aa"+
		"\u00ab\7k\2\2\u00ab\u00ac\7h\2\2\u00ac\22\3\2\2\2\u00ad\u00ae\7k\2\2\u00ae"+
		"\u00af\7p\2\2\u00af\24\3\2\2\2\u00b0\u00b1\7k\2\2\u00b1\u00b2\7p\2\2\u00b2"+
		"\u00b3\7j\2\2\u00b3\u00b4\7g\2\2\u00b4\u00b5\7t\2\2\u00b5\u00b6\7k\2\2"+
		"\u00b6\u00b7\7v\2\2\u00b7\u00b8\7u\2\2\u00b8\26\3\2\2\2\u00b9\u00ba\7"+
		"k\2\2\u00ba\u00bb\7u\2\2\u00bb\u00bc\7x\2\2\u00bc\u00bd\7q\2\2\u00bd\u00be"+
		"\7k\2\2\u00be\u00bf\7f\2\2\u00bf\30\3\2\2\2\u00c0\u00c1\7n\2\2\u00c1\u00c2"+
		"\7g\2\2\u00c2\u00c3\7v\2\2\u00c3\32\3\2\2\2\u00c4\u00c5\7n\2\2\u00c5\u00c6"+
		"\7q\2\2\u00c6\u00c7\7q\2\2\u00c7\u00c8\7r\2\2\u00c8\34\3\2\2\2\u00c9\u00ca"+
		"\7r\2\2\u00ca\u00cb\7q\2\2\u00cb\u00cc\7q\2\2\u00cc\u00cd\7n\2\2\u00cd"+
		"\36\3\2\2\2\u00ce\u00cf\7v\2\2\u00cf\u00d0\7j\2\2\u00d0\u00d1\7g\2\2\u00d1"+
		"\u00d2\7p\2\2\u00d2 \3\2\2\2\u00d3\u00d4\7y\2\2\u00d4\u00d5\7j\2\2\u00d5"+
		"\u00d6\7k\2\2\u00d6\u00d7\7n\2\2\u00d7\u00d8\7g\2\2\u00d8\"\3\2\2\2\u00d9"+
		"\u00da\7e\2\2\u00da\u00db\7c\2\2\u00db\u00dc\7u\2\2\u00dc\u00dd\7g\2\2"+
		"\u00dd$\3\2\2\2\u00de\u00df\7g\2\2\u00df\u00e0\7u\2\2\u00e0\u00e1\7c\2"+
		"\2\u00e1\u00e2\7e\2\2\u00e2&\3\2\2\2\u00e3\u00e4\7p\2\2\u00e4\u00e5\7"+
		"g\2\2\u00e5\u00e6\7y\2\2\u00e6(\3\2\2\2\u00e7\u00e8\7q\2\2\u00e8\u00e9"+
		"\7h\2\2\u00e9*\3\2\2\2\u00ea\u00eb\7v\2\2\u00eb\u00ec\7t\2\2\u00ec\u00ed"+
		"\7w\2\2\u00ed\u00ee\7g\2\2\u00ee,\3\2\2\2\u00ef\u00f0\7h\2\2\u00f0\u00f1"+
		"\7c\2\2\u00f1\u00f2\7n\2\2\u00f2\u00f3\7u\2\2\u00f3\u00f4\7g\2\2\u00f4"+
		".\3\2\2\2\u00f5\u00f6\7>\2\2\u00f6\u00f7\7/\2\2\u00f7\60\3\2\2\2\u00f8"+
		"\u00f9\7-\2\2\u00f9\62\3\2\2\2\u00fa\u00fb\7/\2\2\u00fb\64\3\2\2\2\u00fc"+
		"\u00fd\7,\2\2\u00fd\66\3\2\2\2\u00fe\u00ff\7\61\2\2\u00ff8\3\2\2\2\u0100"+
		"\u0101\7\u0080\2\2\u0101:\3\2\2\2\u0102\u0103\7>\2\2\u0103<\3\2\2\2\u0104"+
		"\u0105\7>\2\2\u0105\u0106\7?\2\2\u0106>\3\2\2\2\u0107\u0108\7?\2\2\u0108"+
		"@\3\2\2\2\u0109\u010a\7p\2\2\u010a\u010b\7q\2\2\u010b\u010c\7v\2\2\u010c"+
		"B\3\2\2\2\u010d\u010e\7\60\2\2\u010eD\3\2\2\2\u010f\u0110\7B\2\2\u0110"+
		"F\3\2\2\2\u0111\u0112\7}\2\2\u0112H\3\2\2\2\u0113\u0114\7\177\2\2\u0114"+
		"J\3\2\2\2\u0115\u0116\7*\2\2\u0116L\3\2\2\2\u0117\u0118\7+\2\2\u0118N"+
		"\3\2\2\2\u0119\u011a\7=\2\2\u011aP\3\2\2\2\u011b\u011c\7<\2\2\u011cR\3"+
		"\2\2\2\u011d\u011e\7.\2\2\u011eT\3\2\2\2\u011f\u0120\7?\2\2\u0120\u0121"+
		"\7@\2\2\u0121V\3\2\2\2\u0122\u0123\7U\2\2\u0123\u0124\7G\2\2\u0124\u0125"+
		"\7N\2\2\u0125\u0126\7H\2\2\u0126\u0127\7a\2\2\u0127\u0128\7V\2\2\u0128"+
		"\u0129\7[\2\2\u0129\u012a\7R\2\2\u012a\u012b\7G\2\2\u012bX\3\2\2\2\u012c"+
		"\u0132\t\2\2\2\u012d\u0131\5g\63\2\u012e\u0131\5_/\2\u012f\u0131\7a\2"+
		"\2\u0130\u012d\3\2\2\2\u0130\u012e\3\2\2\2\u0130\u012f\3\2\2\2\u0131\u0134"+
		"\3\2\2\2\u0132\u0130\3\2\2\2\u0132\u0133\3\2\2\2\u0133Z\3\2\2\2\u0134"+
		"\u0132\3\2\2\2\u0135\u0137\t\3\2\2\u0136\u0135\3\2\2\2\u0137\u0138\3\2"+
		"\2\2\u0138\u0136\3\2\2\2\u0138\u0139\3\2\2\2\u0139\u013f\3\2\2\2\u013a"+
		"\u013e\5g\63\2\u013b\u013e\5_/\2\u013c\u013e\7a\2\2\u013d\u013a\3\2\2"+
		"\2\u013d\u013b\3\2\2\2\u013d\u013c\3\2\2\2\u013e\u0141\3\2\2\2\u013f\u013d"+
		"\3\2\2\2\u013f\u0140\3\2\2\2\u0140\\\3\2\2\2\u0141\u013f\3\2\2\2\u0142"+
		"\u0143\t\4\2\2\u0143^\3\2\2\2\u0144\u0145\t\5\2\2\u0145`\3\2\2\2\u0146"+
		"\u0147\t\6\2\2\u0147b\3\2\2\2\u0148\u0154\5].\2\u0149\u014b\5\63\31\2"+
		"\u014a\u0149\3\2\2\2\u014a\u014b\3\2\2\2\u014b\u014c\3\2\2\2\u014c\u0150"+
		"\5a\60\2\u014d\u014f\5_/\2\u014e\u014d\3\2\2\2\u014f\u0152\3\2\2\2\u0150"+
		"\u014e\3\2\2\2\u0150\u0151\3\2\2\2\u0151\u0154\3\2\2\2\u0152\u0150\3\2"+
		"\2\2\u0153\u0148\3\2\2\2\u0153\u014a\3\2\2\2\u0154d\3\2\2\2\u0155\u0158"+
		"\5+\25\2\u0156\u0158\5-\26\2\u0157\u0155\3\2\2\2\u0157\u0156\3\2\2\2\u0158"+
		"f\3\2\2\2\u0159\u015a\t\7\2\2\u015ah\3\2\2\2\u015b\u015c\7/\2\2\u015c"+
		"\u015d\7/\2\2\u015d\u0161\3\2\2\2\u015e\u0160\13\2\2\2\u015f\u015e\3\2"+
		"\2\2\u0160\u0163\3\2\2\2\u0161\u0162\3\2\2\2\u0161\u015f\3\2\2\2\u0162"+
		"\u0164\3\2\2\2\u0163\u0161\3\2\2\2\u0164\u0165\7\f\2\2\u0165\u0166\3\2"+
		"\2\2\u0166\u0167\b\64\7\2\u0167j\3\2\2\2\u0168\u0169\7\u0080\2\2\u0169"+
		"\u016a\7\u0080\2\2\u016a\u016b\7\u0080\2\2\u016bl\3\2\2\2\u016c\u016e"+
		"\t\b\2\2\u016d\u016c\3\2\2\2\u016e\u016f\3\2\2\2\u016f\u016d\3\2\2\2\u016f"+
		"\u0170\3\2\2\2\u0170\u0171\3\2\2\2\u0171\u0172\b\66\7\2\u0172n\3\2\2\2"+
		"\u0173\u0174\13\2\2\2\u0174\u0175\b\67\b\2\u0175p\3\2\2\2\u0176\u0177"+
		"\7\2\2\3\u0177\u0178\b8\t\2\u0178\u0179\3\2\2\2\u0179\u017a\b8\n\2\u017a"+
		"r\3\2\2\2\u017b\u017c\13\2\2\2\u017c\u017d\7\f\2\2\u017d\u017e\b9\13\2"+
		"\u017e\u017f\3\2\2\2\u017f\u0180\b9\n\2\u0180t\3\2\2\2\u0181\u0182\7$"+
		"\2\2\u0182\u0183\b:\f\2\u0183\u0184\3\2\2\2\u0184\u0185\b:\n\2\u0185v"+
		"\3\2\2\2\u0186\u0187\7^\2\2\u0187\u0188\7v\2\2\u0188\u0189\3\2\2\2\u0189"+
		"\u018a\b;\r\2\u018a\u018b\3\2\2\2\u018b\u018c\b;\3\2\u018cx\3\2\2\2\u018d"+
		"\u018e\7^\2\2\u018e\u018f\7p\2\2\u018f\u0190\3\2\2\2\u0190\u0191\b<\16"+
		"\2\u0191\u0192\3\2\2\2\u0192\u0193\b<\3\2\u0193z\3\2\2\2\u0194\u0195\7"+
		"^\2\2\u0195\u0196\7h\2\2\u0196\u0197\3\2\2\2\u0197\u0198\b=\17\2\u0198"+
		"\u0199\3\2\2\2\u0199\u019a\b=\3\2\u019a|\3\2\2\2\u019b\u019c\7^\2\2\u019c"+
		"\u019d\7d\2\2\u019d\u019e\3\2\2\2\u019e\u019f\b>\20\2\u019f\u01a0\3\2"+
		"\2\2\u01a0\u01a1\b>\3\2\u01a1~\3\2\2\2\u01a2\u01a3\7^\2\2\u01a3\u01a4"+
		"\n\t\2\2\u01a4\u01a5\b?\21\2\u01a5\u01a6\3\2\2\2\u01a6\u01a7\b?\3\2\u01a7"+
		"\u0080\3\2\2\2\u01a8\u01a9\7^\2\2\u01a9\u01ac\7$\2\2\u01aa\u01ac\n\n\2"+
		"\2\u01ab\u01a8\3\2\2\2\u01ab\u01aa\3\2\2\2\u01ac\u01ad\3\2\2\2\u01ad\u01ae"+
		"\b@\22\2\u01ae\u01af\3\2\2\2\u01af\u01b0\b@\3\2\u01b0\u0082\3\2\2\2\u01b1"+
		"\u01b2\7\2\2\3\u01b2\u01b3\bA\23\2\u01b3\u01b4\3\2\2\2\u01b4\u01b5\bA"+
		"\n\2\u01b5\u0084\3\2\2\2\u01b6\u01b7\7*\2\2\u01b7\u01b8\7,\2\2\u01b8\u01b9"+
		"\3\2\2\2\u01b9\u01ba\bB\2\2\u01ba\u01bb\bB\3\2\u01bb\u0086\3\2\2\2\u01bc"+
		"\u01bd\7,\2\2\u01bd\u01be\7+\2\2\u01be\u01bf\3\2\2\2\u01bf\u01c0\bC\n"+
		"\2\u01c0\u01c1\bC\3\2\u01c1\u0088\3\2\2\2\u01c2\u01c3\13\2\2\2\u01c3\u01c4"+
		"\3\2\2\2\u01c4\u01c5\bD\3\2\u01c5\u008a\3\2\2\2\22\2\3\4\u0130\u0132\u0136"+
		"\u0138\u013d\u013f\u014a\u0150\u0153\u0157\u0161\u016f\u01ab\24\7\4\2"+
		"\5\2\2\3\3\2\3\4\3\7\3\2\b\2\2\3\67\4\38\5\6\2\2\39\6\3:\7\3;\b\3<\t\3"+
		"=\n\3>\13\3?\f\3@\r\3A\16";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}