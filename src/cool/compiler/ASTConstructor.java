package cool.compiler;


import cool.parser.CoolParser;
import cool.parser.CoolParserBaseVisitor;
import cool.structures.Primordial;
import cool.structures.Assignment;
import cool.structures.Attribute;
import cool.structures.Block;
import cool.structures.Case;
import cool.structures.CaseBranch;
import cool.structures.Class;
import cool.structures.Complement;
import cool.structures.Dispatch;
import cool.structures.Div;
import cool.structures.Equal;
import cool.structures.Expr;
import cool.structures.Formal;
import cool.structures.Id;
import cool.structures.If;
import cool.structures.ImplicitDispatch;
import cool.structures.IsVoid;
import cool.structures.Less;
import cool.structures.LessEqual;
import cool.structures.Terminal;
import cool.structures.While;
import cool.structures.FunctionCall;
import cool.structures.Let;
import cool.structures.Local;
import cool.structures.Method;
import cool.structures.Minus;
import cool.structures.Mul;
import cool.structures.New;
import cool.structures.Not;
import cool.structures.Plus;
import cool.structures.Scope;
import cool.structures.SymbolTable;

public class ASTConstructor extends CoolParserBaseVisitor<Scope> {
	
	public Primordial program = null;
	
	public Scope visitProgram(CoolParser.ProgramContext ctx) {
		this.program = new Primordial(ctx);
		
		for (var child : ctx.classDeclaration()) {
			Class clasa = (Class) visit(child);
			if (clasa != null) {
				this.program.addClass(clasa);
			}
		}
		
//			Legam clasele de parintii lor, iar pentru ca suntem in exitProgram,
//		toate au fost deja definite
		for (var clasa : program.getClasses().values()) {
			if (clasa.getParentName() != null) {
				clasa.setParent(program.getClass(clasa.getParentName()));
			}
		}
		
		for (var clasa : program.getClasses().values()) {
			SymbolTable.globals.addType(clasa.getType());
		}
		
		SymbolTable.primordial = program;

		return program;
	}
	
	public Scope visitClassDeclaration(CoolParser.ClassDeclarationContext ctx) {
		String className = ctx.className.getText();
		String classInherited = null;
		if (ctx.classInherited != null) {
			classInherited = ctx.classInherited.getText();
		}
		
		Class cls = new Class(
			className,
			classInherited,
			ctx
		);
		
		if (cls.getError() != null) {
			SymbolTable.addError(cls.getError());
			return null;
		}
		
		for (var child : ctx.classBody().feature()) {
			Scope feature = visit(child);
			if (feature instanceof Attribute) {
				cls.addAttribute((Attribute) feature);
			}
			else if (feature instanceof Method) {
				cls.addMethod((Method) feature);
			}
		}
		
		return cls;
	}
	
	public Scope visitAttribute(CoolParser.AttributeContext ctx) {
		String attrName = ctx.attrName.getText();
		String attrType = ctx.attrType.getText();
		
		Attribute attribute = new Attribute(
			attrName,
			attrType,
			ctx
		);
		
		if (ctx.attrInitValue != null) {
			attribute.setIinitialValue((Expr) visit(ctx.attrInitValue));
		}
		
		return attribute;
	}
	
	public Scope visitMethod(CoolParser.MethodContext ctx) {
		String methodName = ctx.methodName.getText();
		String methodType = ctx.methodType.getText();
		
		Method method = new Method(
			methodName,
			methodType,
			ctx
		);
		
		for (var child : ctx.methodHeader().formal()) {
			method.addFormal((Formal) visit(child));
		}
		method.setMethodBody((Expr) visit(ctx.methodBody().expr()));
		
		return method;
	}
	
	public Scope visitFormal(CoolParser.FormalContext ctx) {
		String formalName = ctx.formalName.getText();
		String formalType = ctx.formalType.getText();
		
		Formal formal = new Formal(
			formalName,
			formalType,
			ctx
		);
		
		return formal;
	}
	
	public Scope visitLet(CoolParser.LetContext ctx) {
		Let let = new Let(ctx);
		
		for (var child : ctx.vars().local()) {
			let.addLocal((Local) visit(child));
		}
		let.setLetBody((Expr) visit(ctx.body));
		
		return let;
	}
	
	public Scope visitLocal(CoolParser.LocalContext ctx) {
		String localName = ctx.varName.getText();
		String localType = ctx.varType.getText();
		
		Local local = new Local(localName, localType, ctx);
		if (ctx.varInitValue != null) {
			local.setInitialValue((Expr) visit(ctx.varInitValue));
		}
		
		return local;
	}
	
	public Scope visitCase(CoolParser.CaseContext ctx) {
		Case caz = new Case(ctx);
		
		caz.setCondition((Expr) visit(ctx.cond));
		for (var child : ctx.caseBranches().caseBranch()) {
			caz.addCaseBranch((CaseBranch) visit(child));
		}
		
		return caz;
	}
	
	public Scope visitCaseBranch(CoolParser.CaseBranchContext ctx) {
		String caseBranchName = ctx.varName.getText();
		String caseBranchType = ctx.varType.getText();
		
		CaseBranch caseBranch = new CaseBranch(caseBranchName, caseBranchType, ctx);
		caseBranch.setImpliedExpr((Expr) visit(ctx.expr()));
		
		return caseBranch;
	}

	public Scope visitBracket(CoolParser.BracketContext ctx) {
		return visit(ctx.expr());
	}
	
	public Scope visitBlock(CoolParser.BlockContext ctx) {
		Block block = new Block(ctx);
		
		for (var child : ctx.body.expr()) {
			block.addStatement((Expr) visit(child));
		}
		
		return block;
	}
	
	public Scope visitPlus(CoolParser.PlusContext ctx) {
		Plus plus = new Plus(ctx);
		
		plus.setLeft((Expr) visit(ctx.leftExpr));
		plus.setRight((Expr) visit(ctx.rightExpr));
				
		return plus;
	}
	
	public Scope visitMinus(CoolParser.MinusContext ctx) {
		Minus minus = new Minus(ctx);
		
		minus.setLeft((Expr) visit(ctx.leftExpr));
		minus.setRight((Expr) visit(ctx.rightExpr));
		
		return minus;
	}
	
	public Scope visitMul(CoolParser.MulContext ctx) {
		Mul mul = new Mul(ctx);
		
		mul.setLeft((Expr) visit(ctx.leftExpr));
		mul.setRight((Expr) visit(ctx.rightExpr));
		
		return mul;
	}
	
	
	public Scope visitDiv(CoolParser.DivContext ctx) {
		Div div = new Div(ctx);
		
		div.setLeft((Expr) visit(ctx.leftExpr));
		div.setRight((Expr) visit(ctx.rightExpr));
		
		return div;
	}
	
	public Scope visitComplement(CoolParser.ComplementContext ctx) {
		Complement complement = new Complement(ctx);
		
		complement.setRight((Expr) visit(ctx.expr()));
		
		return complement;
	}
	
	public Scope visitEqual(CoolParser.EqualContext ctx) {
		Equal equal = new Equal(ctx);
		
		equal.setLeft((Expr) visit(ctx.leftExpr));
		equal.setRight((Expr) visit(ctx.rightExpr));
		
		return equal;
	}
	
	public Scope visitLess(CoolParser.LessContext ctx) {
		Less less = new Less(ctx);
		
		less.setLeft((Expr) visit(ctx.leftExpr));
		less.setRight((Expr) visit(ctx.rightExpr));
		
		return less;
	}
	
	public Scope visitLessEqual(CoolParser.LessEqualContext ctx) {
		LessEqual lessEqual = new LessEqual(ctx);
		
		lessEqual.setLeft((Expr) visit(ctx.leftExpr));
		lessEqual.setRight((Expr) visit(ctx.rightExpr));
		
		return lessEqual;
	}
	
	public Scope visitNot(CoolParser.NotContext ctx) {
		Not not = new Not(ctx);
		
		not.setRight((Expr) visit(ctx.expr()));
		
		return not;
	}
	
	public Scope visitAssignment(CoolParser.AssignmentContext ctx) {
		Assignment assignment = new Assignment(ctx);
		
		assignment.setLeft((Id) visit(ctx.leftExpr));
		assignment.setRight((Expr) visit(ctx.rightExpr));
		
		return assignment;
	}
	
	public Scope visitNew(CoolParser.NewContext ctx) {
		New n = new New(ctx);
		
		n.setNewType(ctx.type(), ctx.type().getText());
		
		return n;
	}
	
	public Scope visitIsvoid(CoolParser.IsvoidContext ctx) {
		IsVoid isVoid = new IsVoid(ctx);
		
		isVoid.setRight((Expr) visit(ctx.expr()));
		
		return isVoid;
	}
	
	public Scope visitWhile(CoolParser.WhileContext ctx) {
		While w = new While(ctx);
		
		w.setWhileCondition(((Expr) visit(ctx.cond)));
		w.setWhileBody((((Expr) visit(ctx.body))));
		
		return w;
	}
	
	public Scope visitIf(CoolParser.IfContext ctx) {
		If i = new If(ctx);
		
		i.setIfCondition((Expr) visit(ctx.cond));
		i.setIfBody(((Expr) visit(ctx.body1)));
		i.setIfElse((Expr) visit(ctx.body2));
		
		return i;
	}
	
	public Scope visitDispatch(CoolParser.DispatchContext ctx) {
		Dispatch disp;
		
		if (ctx.cast() != null) {
			disp = new Dispatch(ctx.cast().type().getText(), ctx);
		} else {
			disp = new Dispatch(null, ctx);
		}
		
		disp.setCls((Expr) visit(ctx.expr()));
		disp.setFunctionCall((FunctionCall) visit(ctx.functionCall()));
		
		return disp;
	}
	
	public Scope visitFunctionCall(CoolParser.FunctionCallContext ctx) {
		FunctionCall funcCall = new FunctionCall(ctx.functionName.getText(), ctx);
		
		
		for (var child : ctx.expr()) {
			funcCall.addArgument((Expr) visit(child));
		}
		
//		System.out.println(funcCall);
		return funcCall;
	}
	
	public Scope visitImplicitDispatch(CoolParser.ImplicitDispatchContext ctx) {
		ImplicitDispatch disp = new ImplicitDispatch(ctx);
		
		disp.setFunctionCall((FunctionCall) visit(ctx.functionCall()));
		
		return disp;
	}
	
	
	public Scope visitId(CoolParser.IdContext ctx) {
		String idName = ctx.ID().getText();
		
		Id id = new Id(idName, ctx);
		
		return id;
	}
	
	public Scope visitBool(CoolParser.BoolContext ctx) {
		String boolValue = ctx.getText();
		
		Terminal bool = new Terminal(boolValue, "Bool", ctx);
		
		return bool;
	}
	
	public Scope visitInteger(CoolParser.IntegerContext ctx) {
		String intValue = ctx.INTEGER().getText();
		
		Terminal integer = new Terminal(intValue, "Int", ctx);
		
		return integer;
	}
	
	public Scope visitString(CoolParser.StringContext ctx) {
		String stringValue = ctx.STRING().getText();
		
		Terminal string = new Terminal(stringValue, "String", ctx);
		
		return string;
	}
}
