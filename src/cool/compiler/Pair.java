package cool.compiler;

import org.antlr.v4.runtime.Token;

public class Pair {
	private Token first;
	private Token second;
	
	Pair(Token first, Token second) {
		this.first = first;
		this.second = second;
	}
	
	public Token getFirst() {
		return this.first;
	}
	
	public Token getSecond() {
		return this.second;
	}
}
