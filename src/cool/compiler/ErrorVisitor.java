package cool.compiler;

import cool.structures.Scope;
import cool.structures.SymbolTable;

public class ErrorVisitor {
	public void visit(Scope element) {
		element.endErrors();
		SymbolTable.addError(element.getError());
	}
}
