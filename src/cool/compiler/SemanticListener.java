//package cool.compiler;
//
//
//import java.util.HashMap;
//import java.util.LinkedList;
//import java.util.Stack;
//
//import cool.parser.CoolParser;
//import cool.parser.CoolParserBaseListener;
//import cool.structures.*;
//import cool.structures.Class;
//import cool.structures.Case;
//
//import org.antlr.v4.runtime.ParserRuleContext;
//
//public class SemanticListener extends CoolParserBaseListener {
//	
////	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- CAPITOLUL 1 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
///*
// * 		Si la inceput au fost clasele, fara de care Cool n-ar fi putut exista. Dar clasele nu
// * 	puteau purta orice nume doreau, nu se punea problema ca ele sa nu-si cunoasca parintele
// * 	si nici nu puteau a face copii unele dintr-aletele.
// */
//	
//	private HashMap<String, Class> classes = new HashMap<>();
//	
//	public void enterClassDeclaration(CoolParser.ClassDeclarationContext ctx) {
////		Obtinem numele clasei si pe al parintelui (daca exista)
//		String className = ctx.className.getText();
//		String parentName = null;
//		if (null != ctx.classInherited) {
//			parentName = ctx.classInherited.getText();
//		}
//		
////		Cream o 'Clasa' si o adaugam in lista de clase
//		Class cls = new Class(className, parentName, ctx);
//		classes.put(className, cls);
////		Adaugam clasa si in globals
//		SymbolTable.globals.add(new Symbol(className));
//	}
//	
//	
//	
//	
////	
////	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- TASK 2 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
///*
// * 		Iara dupa ce clasele au aparut in Cool, atributele n-au intarziat sa apara. Ca si clasele,
// * 	atributele nu puteau sa-si poarte numele lor, dar nici pe ale altora din generatia lor, cat si
// * 	din generatiile anterioare in semn de respect pentru stramosi.
// */
//	
//	public void enterAttribute(CoolParser.AttributeContext ctx) {
//		String attrName = ctx.attrName.getText();
//		String attrType = ctx.attrType.getText();
//		var classContext = (CoolParser.ClassDeclarationContext) ctx.getParent().getParent().getParent();
//		var clasa = classes.get(classContext.className.getText());
//		
//		Attribute attr = new Attribute(attrName, attrType, clasa, ctx);
//		clasa.addAttribute(attr);
//	}
//
////	
////	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- TASK 3 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
///*
// * 		
// */
//	
//	public void enterMethod(CoolParser.MethodContext ctx) {
//		String methodName = ctx.methodName.getText();
//		String methodType = ctx.methodType.getText();
//		var classContext = (CoolParser.ClassDeclarationContext) ctx.getParent().getParent().getParent();
//		var clasa = classes.get(classContext.className.getText());
//		
//		Method method = new Method(methodName, methodType, clasa, ctx);
////		if (! method.foundErrors) {
////			clasa.addMethod(method);
////		}
//		clasa.addMethod(method);
//	}
//	
//	public void enterFormal(CoolParser.FormalContext ctx) { 
//		String formalName = ctx.formalName.getText();
//		String formalType = ctx.formalType.getText();
//		var methodContext = (CoolParser.MethodContext) ctx.getParent().getParent();
//		var classContext = (CoolParser.ClassDeclarationContext) methodContext.getParent().getParent().getParent();
//		
//		var clasa = classes.get(classContext.className.getText());
//		var metoda = clasa.getMethod(methodContext.methodName.getText());
//		
//		Formal formal = new Formal(formalName, formalType, metoda, ctx);
//		metoda.addFormal(formal);
//	}
//
////	
////	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- TASK 4 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
///*
// * 		
// */
//	
//	public void enterLet(CoolParser.LetContext ctx) {
//		Scope parent = magicTraceBuilder(ctx);
//		Let let = new Let(ctx);
//		
//		if (parent instanceof Method) {
//			((Method) parent).setMethodBody(let);
//		}
//	}
//	
//	public void enterLocal(CoolParser.LocalContext ctx) {
//		String localName = ctx.varName.getText();
//		String localType = ctx.varType.getText();
//		
//		Let parent = (Let) magicTraceBuilder(ctx);
//		
//		Local local = new Local(localName, localType, parent, ctx);
//		parent.addLocal(local);
//	}
//
////	
////	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- TASK 5 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
///*
// * 		
// */
//	
//	public void enterCase(CoolParser.CaseContext ctx) {
//		Scope parent = magicTraceBuilder(ctx);
//		
//		Case caz = new Case(ctx);
//		
//		if (parent instanceof Method) {
//			((Method) parent).setMethodBody(caz);
//		}
//		else if (parent instanceof Let) {
//			((Let) parent).setLetBody(caz);
//		}
//	}
//	
//	public void enterCaseBranch(CoolParser.CaseBranchContext ctx) {
//		String caseBranchName = ctx.varName.getText();
//		String caseBranchType = ctx.varType.getText();
//		
//		Case parent = (Case) magicTraceBuilder(ctx);
//		
//		CaseBranch caseBranch = new CaseBranch(caseBranchName, caseBranchType, parent, ctx);
//		parent.addCaseBranch(caseBranch);
//	}
//	
////	
////	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- TASK 6 -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
///*
// * 		
// */
//	
//	public void enterId(CoolParser.IdContext ctx) {
//		String idName = ctx.getText();
//		
//		Scope parent = magicTraceBuilder(ctx);
//		Id id = new Id(idName, parent, ctx);
//		
//		if (parent instanceof Local) {
//			((Local) parent).setInitialValue(id);
//		}
//	}
//	
////	
////	-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=- FINAL -=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=-=
///*
// * 		
// */
//	
//	public void exitProgram(CoolParser.ProgramContext ctx) {
//	//		Legam clasele de parintii lor, iar pentru ca suntem in exitProgram,
//	//	toate au fost deja definite
//		for (String cls : classes.keySet()) {
//			Class clasa = classes.get(cls);
//			if (clasa.getParentName() != null) {
//				clasa.setParent(classes.get(clasa.getParentName()));
//			}
//		}
//		
////		Verificam daca in momentul rularii mai pot aparea alte erori decat cele initiale in elementele programului
//		ErrorVisitor visitor = new ErrorVisitor();
//		for (var cls : classes.values()) {
//			cls.accept(visitor);
//		}
//	}
//	
//	private Scope magicTraceBuilder(ParserRuleContext currentContext) {
//		Stack<ParserRuleContext> stack = new Stack<>();
//		ParserRuleContext ctx = currentContext;
//		
//		while (!(ctx instanceof CoolParser.ProgramContext)) {
//			stack.push(ctx);
//			ctx = ctx.getParent();
//		}
//		
//		LinkedList<Scope> trace = new LinkedList<>();
//		while (stack.size() > 1) {
//			ctx = stack.pop();
//			
//			if (ctx instanceof CoolParser.ClassDeclarationContext) {
//				Class cls = classes.
//						get(((CoolParser.ClassDeclarationContext) ctx).className.getText());
//				trace.addLast(cls);
//			}
//			else if (ctx instanceof CoolParser.MethodContext) {
//				Method method = ((Class) trace.peekLast()).
//						getMethod(((CoolParser.MethodContext) ctx).methodName.getText());
//				trace.addLast(method);
//			}
//			else if (ctx instanceof CoolParser.LetContext) {
//				Scope last = trace.getLast();
//				if (last instanceof Method) {
//					Let let = (Let) ((Method) last).getMethodBody();
//					trace.addLast(let);
//				}
//			}
//			else if (ctx instanceof CoolParser.CaseContext) {
//				Scope last = trace.getLast();
//				if (last instanceof Method) {
//					Case caz = (Case) ((Method) last).getMethodBody();
//					trace.addLast(caz);
//				}
//				else if (last instanceof Let) {
//					Expr let = ((Let) last).getLetBody();
//					trace.addLast(let);
//				}
//			}
//			else if (ctx instanceof CoolParser.LocalContext) {
//				Local local = ((Let) trace.peekLast()).
//						getLocal(((CoolParser.LocalContext) ctx).varName.getText());
//				trace.addLast(local);
//			}
//		}
//		
//		if (trace.isEmpty()) {
//			return null;
//		}
//		return trace.peekLast();
//	}
//}
